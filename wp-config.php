<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'amenity_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fl5f3{1P+FHTj 6iXyvG*mexV[l|;>Dp{Z([xNvJ#a8F=wZK.UdPy*aY*j.SX4V7');
define('SECURE_AUTH_KEY',  's`-*l87aI!#9A2*}S<l4Y<0!NK,4w#bpJ iq}3hl[GUX)r$ErGf@*la,vpNq0.sx');
define('LOGGED_IN_KEY',    '2wnEJ8;bHok>_2^qNssK6L2)0WJ >LDd|4Pd5U]J8/Rwgj8J@vfALlO0|}]-+$]]');
define('NONCE_KEY',        'W(z(V4|}0#(MWc!GnnP%#LNfxt^wh<n*NtHpeObby@A4$4-21K/}8JYn@{]lq{{4');
define('AUTH_SALT',        '2l8z$ huw0=LI`w7F3ynU_N|Dmt~CIDV|n(L&b^0(P=8G+~WNoAv^OAC=xSqXqP}');
define('SECURE_AUTH_SALT', 'a36>ab_xcKj04uk&X1Y=bF&h$AMHx#]{e/+BNzAf)[4xp-.2sg9}OLToMIm@}[!s');
define('LOGGED_IN_SALT',   'cIOpFIe36I0-b#x4NU,ks; >`ak;:NcIpdaOT8v2z./r%uOyJExr9jTJXg5/+hW{');
define('NONCE_SALT',       'NR6~;.Tdhm?O,`@ TQi4dHOn5[xMJ=TNTqu$Y9@Xc`*=cKTyQmnsI`&6<JqB[P:V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'amenity_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
