<?php
global $product;

$is_on_sale = !empty($product->get_sale_price()) ? true : false;
$image_link = wp_get_attachment_url( get_post_thumbnail_id() );

?>
<!--Product item-->
<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="product-thumb transition">
        <a class="quickview" data-rel="details" href="#quickview_1">

        </a>
        <div class="quick_info">
            <div id="quickview_1" class="quickview-style">
                <div>
                    <div class="left col-sm-4">
                        <div class="quickview_image image">
                            <a href="#0">
                                <img alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>" class="img-responsive" src="<?php echo $image_link;?>" />
                            </a>
                        </div>
                    </div>
                    <div class="right col-sm-8">
                        <h2><?php echo get_the_title(); ?></h2>
                        <div class="inf">
                            <p class="quickview_manufacture manufacture manufacture">Brand: <a href="#0">libero convallis</a></p>
                            <p class="product_model model">Model: <?php echo $product->get_sku(); ?> </p>

                            <div class="price">
                                <span class="price-new"><?php echo $product->get_price(); ?> VND</span>
                                <?php if($is_on_sale): ?>
                                <span class="price-old"><?php echo $product->get_regular_price(); ?> VND</span>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="cart-button">
                            <button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('47');"><i class="fa fa-shopping-cart"></i></button>
                            <button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');"><i class="fa fa-bar-chart"></i></button>
                            <button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');"><i class="fa fa-star"></i></button>
                        </div>
                        <div class="clear"></div>
                        <div class="rating">
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="quickview_description description">

                            <p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

                            <p>
                                <b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($is_on_sale) { ?>
        <div class="sale">Sale</div>
        <?php } ?>
        <div class="image">
            <a class="lazy" style="padding-bottom: 100%"
               href="#0">
                <img alt="Consectetur adipiscing"
                     title="Consectetur adipiscing"
                     class="img-responsive"
                     data-src="<?php echo get_the_post_thumbnail_url(); ?>"
                     src="#" />
            </a>
        </div>
        <div class="caption">
            <div class="name"><a href=""><?php echo get_the_title(); ?></a></div>
            <div class="description">
                dasdadasdasdasd
            </div>
        </div>
        <div class="wrap">
            <div class="price">
                <span class="price-new"><?php echo $product->get_price(); ?> VND</span>
                <?php if($is_on_sale): ?>
                    <span class="price-old"><?php echo $product->get_regular_price(); ?> VND</span>
                <?php endif;?>
            </div>
            <div class="cart-button">
                <button class="product-btn-add" type="button" onclick="cart.add('47');"><i class="fa fa-shopping-cart"></i></button>
                <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');"><i class="fa fa-bar-chart"></i></button>
                <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');"><i class="fa fa-star"></i></button>
            </div>
        </div>

    </div>
</div>
<!--End product item-->
