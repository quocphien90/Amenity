<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Category
 *
 * @package amenity
 */

get_header(); ?>


    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#0"><i class="fa fa-home"></i></a></li>
            <li><a href="#0">HARDWOOD</a></li>
        </ul>
        <div class="row">
            <?php echo do_action("amenity_sidebar") ?>

            <!-- Product list -->
            <div id="content" class="col-sm-9">
                <!-- Sub Categories -->
                <div class="product-filter clearfix">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="input-sort">Sort By:</label>
                        </div>
                        <div class="col-md-3">
                            <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=pd.name&amp;order=ASC">Name (A - Z)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=pd.name&amp;order=DESC">Name (Z - A)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=p.price&amp;order=ASC">Price (Low &gt; High)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=p.price&amp;order=DESC">Price (High &gt; Low)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=rating&amp;order=DESC">Rating (Highest)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=rating&amp;order=ASC">Rating (Lowest)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=p.model&amp;order=ASC">Model (A - Z)</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;sort=p.model&amp;order=DESC">Model (Z - A)</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label" for="input-limit">Show:</label>
                        </div>
                        <div class="col-md-2">
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=6" selected="selected">6</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=25">25</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=50">50</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=75">75</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=100">100</option>
                            </select>
                        </div>
                        <div class="col-md-3 text-right">
                            <div class="button-view">
                                <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                                <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-cat clearfix">
                    <div class="pull-left">
                        <ul class="pagination">
                            <li class="active"><span>1</span></li>
                            <li><a href="#0">2</a></li>
                            <li><a href="#0">&gt;</a></li>
                            <li><a href="#0">&gt;|</a></li>
                        </ul>
                    </div>
                    <div class="pull-left nam-page">Showing 1 to 6 of 6 (1 Pages)</div>
                </div>
                <div class="row">

                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Aliquam eget"
                                         title="Aliquam eget"
                                         class="img-responsive"
                                         data-src="images/catalog/product-10-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Aliquam eget</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('28');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('28');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('28');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Consectetur adipiscing"
                                         title="Consectetur adipiscing"
                                         class="img-responsive"
                                         data-src="images/catalog/product-10-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Consectetur adipiscing</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$60.00</span> <span class="price-old">$100.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('47');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Dolor sit amet"
                                         title="Dolor sit amet"
                                         class="img-responsive"
                                         data-src="images/catalog/product-13-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Dolor sit amet</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('30');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Donec non posuere"
                                         title="Donec non posuere"
                                         class="img-responsive"
                                         data-src="images/catalog/product-34-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Donec non posuere</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$90.00</span> <span class="price-old">$1,000.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('44');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('44');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('44');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Lorem ipsum"
                                         title="Lorem ipsum"
                                         class="img-responsive"
                                         data-src="images/catalog/product-22-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Lorem ipsum</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$90.00</span> <span class="price-old">$100.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('42');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Sed vitae enim"
                                         title="Sed vitae enim"
                                         class="img-responsive"
                                         data-src="images/catalog/product-40-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Sed vitae enim</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        $80.00
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('31');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('31');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('31');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Donec non posuere"
                                         title="Donec non posuere"
                                         class="img-responsive"
                                         data-src="images/catalog/product-34-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Donec non posuere</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$90.00</span> <span class="price-old">$1,000.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('44');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('44');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('44');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Lorem ipsum"
                                         title="Lorem ipsum"
                                         class="img-responsive"
                                         data-src="images/catalog/product-22-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Lorem ipsum</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        <span class="price-new">$90.00</span> <span class="price-old">$100.00</span>
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('42');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="lazy" style="padding-bottom: 100%"
                                   href="#0">
                                    <img alt="Sed vitae enim"
                                         title="Sed vitae enim"
                                         class="img-responsive"
                                         data-src="images/catalog/product-40-220x220.png"
                                         src="#" />
                                </a>
                            </div>
                            <div>
                                <div class="caption">
                                    <div class="name name-product"><a href="#0">Sed vitae enim</a></div>
                                    <div class="description">
                                        Would you like to set a cozy atmosphere in your house and add an elegant touch..
                                    </div>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    </div>
                                </div>
                                <div class="wrap">
                                    <div class="price price-product">
                                        $80.00
                                    </div>
                                    <div class="cart-button">
                                        <button class="product-btn-add" type="button" onclick="cart.add('31');"><i class="fa fa-shopping-cart"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('31');"><i class="fa fa-exchange"></i></button>
                                        <button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('31');"><i class="fa fa-star"></i></button>
                                    </div>
                                </div>


                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <ul class="pagination">
                            <li class="active"><span>1</span></li>
                            <li><a href="#0">2</a></li>
                            <li><a href="#0">&gt;</a></li>
                            <li><a href="#0">&gt;|</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right">Showing 1 to 6 of 6 (1 Pages)</div>
                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
