<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package storefront
 */
define("HOTEL_PRODUCT_CATEGORY", 24);
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $args = array(
        'orderby'    => 'term_id',
        'order'      => 'ASC',
    );
    $product_categories = get_terms( 'product_cat', $args );
    ?>
    <aside id="column-left" class="col-sm-3 ">
        <div class="box category col-sm-3">
            <div class="box-heading"><h3>Categories</h3></div>
            <div class="box-content">
                <div class="box-category">
                    <ul class="list-unstyled category_menu">
                        <?php
                        if(!empty($product_categories) && count($product_categories) > 0) {
                            foreach ($product_categories as $product_category) :?>
                                <li>
                                    <a href="<?php echo get_term_link($product_category->term_id); ?>"><?php echo esc_html($product_category->name); ?></a>
                                </li>
                            <?php endforeach;
                        }?>
                    </ul>
                </div>
            </div>
        </div>

    </aside>
    <?php
    return;
} else {
?>

<div id="secondary" class="widget-area" role="complementary">
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
<?php } ?>