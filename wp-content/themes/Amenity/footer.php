<?php
/**
 * Functions hooked in to storefront_footer action
 *
 * @hooked amenity_footer_navigation  0
 */
do_action( 'amenity_footer' ); ?>
<?php wp_footer(); ?>
</div>
</body>
</html>