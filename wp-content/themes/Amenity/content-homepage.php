<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package Amenity
 */

$slides = carbon_get_post_meta(get_the_ID(), 'homepage_slide', 'complex');
$feature_categories = carbon_get_post_meta(get_the_ID(), 'homepage_featured_category', 'complex');
?>

	<div class="header_modules">
		<div class="fluid_container">
			<div class="camera_container">
				<div id="camera_wrap_0">
					<?php
					if ( $slides ) {
						$count = 1;
						foreach ($slides as $slide) {
					?>
							<div title="slide-<?php echo $count;?>" data-thumb="<?php echo $slide['slide_photo']; ?>" data-src="<?php echo $slide['slide_photo']; ?>">
							</div>
					<?php
							$count++;
						}
					}
					?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="container">
			<div id="banner0" class="banners row">
				<?php
					if($feature_categories) {
						$count = 1;
						foreach ($feature_categories as $category) {
					?>
							<div class="col-sm-4 banner-<?php echo $count;?>">
								<div class="banner-box">
									<img src="<?php echo $category['homepage_category_photo']; ?>" alt="banner-1" class="img-responsive" />
									<div class="s-desc">
										<h2><?php echo $category['homepage_category_title']; ?></h2>
										<!--<h2>dolore ipsum come</h2>-->
										<a href="#0" class="link"></a>
									</div>
								</div>
							</div>
					<?php
					$count++;
						}
					}
				?>
			</div>
		</div>
	</div>

	<div id="container">
		<div class="container">
			<div class="row">
				<?php do_action( 'amenity_sidebar' ); ?>
				<div id="content" class="col-sm-9">
					<div class="box featured">
						<div class="box-heading"><h3>Featured</h3></div>
						<div class="box-content">
							<div class="row">
								<?php
									$args = array( "limit" => 2 );
									do_action("homepage_content", $args);
								?>
								<!--Product item-->
								<!--	<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_1">

										</a>
										<div class="quick_info">
											<div id="quickview_1" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image">
															<a href="#0">
																<img alt="Consectetur adipiscing" title="Consectetur adipiscing" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-10-270x270.png" />
															</a>
														</div>
													</div>
													<div class="right col-sm-8">
														<h2>Consectetur adipiscing</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="#0">libero convallis</a></p>
															<p class="product_model model">Model: Product 21</p>

															<div class="price">
																<span class="price-new">$60.00</span> <span class="price-old">$100.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('47');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="#0">
												<img alt="Consectetur adipiscing"
													 title="Consectetur adipiscing"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-10-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=47">Consectetur adipiscing</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$60.00</span> <span class="price-old">$100.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('47');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>-->
								<!--End product item-->
								<!--<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_2">

										</a>
										<div class="quick_info">
											<div id="quickview_2" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=30"><img alt="Dolor sit amet" title="Dolor sit amet" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-13-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Dolor sit amet</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=9">Quisque sodales</a></p>
															<p class="product_model model">Model: Product 3</p>

															<div class="price">
																<span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('30');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=30">
												<img alt="Dolor sit amet"
													 title="Dolor sit amet"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-13-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=30">Dolor sit amet</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('30');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_3">

										</a>
										<div class="quick_info">
											<div id="quickview_3" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=35"><img alt="Aenean viverra" title="Aenean viverra" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-46-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Aenean viverra</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=5">Donec eu</a></p>
															<p class="product_model model">Model: Product 8</p>

															<div class="price">
																$100.00
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('35');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('35');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('35');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d�cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=35">
												<img alt="Aenean viverra"
													 title="Aenean viverra"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-46-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=35">Aenean viverra</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												$100.00
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('35');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('35');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('35');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_4">

										</a>
										<div class="quick_info">
											<div id="quickview_4" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=33"><img alt="Aliquam dolor tellus" title="Aliquam dolor tellus" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-52-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Aliquam dolor tellus</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=10">Fusce vestibulum</a></p>
															<p class="product_model model">Model: Product 6</p>

															<div class="price">
																<span class="price-new">$150.00</span> <span class="price-old">$200.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('33');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('33');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('33');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=33">
												<img alt="Aliquam dolor tellus"
													 title="Aliquam dolor tellus"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-52-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=33">Aliquam dolor tellus</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$150.00</span> <span class="price-old">$200.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('33');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('33');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('33');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_5">

										</a>
										<div class="quick_info">
											<div id="quickview_5" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=28"><img alt="Aliquam eget" title="Aliquam eget" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-10-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Aliquam eget</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=5">Donec eu</a></p>
															<p class="product_model model">Model: Product 1</p>

															<div class="price">
																<span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('28');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('28');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('28');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=28">
												<img alt="Aliquam eget"
													 title="Aliquam eget"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-10-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=28">Aliquam eget</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$80.00</span> <span class="price-old">$100.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('28');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('28');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('28');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_6">

										</a>
										<div class="quick_info">
											<div id="quickview_6" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=43"><img alt="Quisque eget" title="Quisque eget" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-31-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Quisque eget</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a></p>
															<p class="product_model model">Model: Product 16</p>

															<div class="price">
																<span class="price-new">$400.00</span> <span class="price-old">$500.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('43');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('43');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('43');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=43">
												<img alt="Quisque eget"
													 title="Quisque eget"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-31-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=43">Quisque eget</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$400.00</span> <span class="price-old">$500.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('43');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('43');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('43');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_7">

										</a>
										<div class="quick_info">
											<div id="quickview_7" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=45"><img alt="Phasellus vel scelerisque" title="Phasellus vel scelerisque" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-37-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Phasellus vel scelerisque</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a></p>
															<p class="product_model model">Model: Product 18</p>

															<div class="price">
																<span class="price-new">$150.00</span> <span class="price-old">$200.00</span>
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('45');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('45');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('45');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="sale">Sale</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=45">
												<img alt="Phasellus vel scelerisque"
													 title="Phasellus vel scelerisque"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-37-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=45">Phasellus vel scelerisque</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												<span class="price-new">$150.00</span> <span class="price-old">$200.00</span>
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('45');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('45');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('45');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_8">

										</a>
										<div class="quick_info">
											<div id="quickview_8" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=48"><img alt="Praesent imperdiet" title="Praesent imperdiet" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-19-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Praesent imperdiet</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a></p>
															<p class="product_model model">Model: product 20</p>

															<div class="price">
																$100.00
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('48');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('48');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('48');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=48">
												<img alt="Praesent imperdiet"
													 title="Praesent imperdiet"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-19-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=48">Praesent imperdiet</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												$100.00
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('48');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('48');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('48');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>

								<div class="product-layout col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="product-thumb transition">
										<a class="quickview" data-rel="details" href="#quickview_9">

										</a>
										<div class="quick_info">
											<div id="quickview_9" class="quickview-style">
												<div>
													<div class="left col-sm-4">
														<div class="quickview_image image"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=34"><img alt="Praesent sodales" title="Praesent sodales" class="img-responsive" src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-25-270x270.png" /></a></div>
													</div>
													<div class="right col-sm-8">
														<h2>Praesent sodales</h2>
														<div class="inf">
															<p class="quickview_manufacture manufacture manufacture">Brand: <a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a></p>
															<p class="product_model model">Model: Product 7</p>

															<div class="price">
																$100.00
															</div>
														</div>
														<div class="cart-button">
															<button class="btn btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('34');"><i class="fa fa-shopping-cart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('34');"><i class="fa fa-bar-chart"></i></button>
															<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('34');"><i class="fa fa-star"></i></button>
														</div>
														<div class="clear"></div>
														<div class="rating">
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
															<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
														</div>

													</div>
													<div class="col-sm-12">
														<div class="quickview_description description">

															<p><b>Would you like to set a cozy atmosphere</b> in your house and add an elegant touch to its interior? Then, don't hesitate to visit our store providing a rich assortment of flooring materials available in different colors and patterns. Regardless of the d&#233;cor of your rooms, you are guaranteed to find something special that will match your furnishings.</p>

															<p>
																<b>Our offerings are just the job</b> for homeowners who are very busy with work and household duties, as their maintenance requires minimal efforts. Using them, you won't need to worry about common flooring problems such as scratches, dents, moisture and fading. If you spill something on their surface, you'll be able to clean stains easily. Just mop a little, and your floor will have a shining look.
															</p>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="image">
											<a class="lazy" style="padding-bottom: 100%"
											   href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=34">
												<img alt="Praesent sodales"
													 title="Praesent sodales"
													 class="img-responsive"
													 data-src="<?php /*echo get_template_directory_uri(); */?>/images/catalog/product-25-270x270.png"
													 src="#" />
											</a>
										</div>
										<div class="caption">
											<div class="name"><a href="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/product&amp;product_id=34">Praesent sodales</a></div>
											<div class="description">
												Would you like to set a cozy atmosphere in your house and add an elegant touch..
											</div>
											<div class="rating">
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
												<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
											</div>
										</div>
										<div class="wrap">
											<div class="price">
												$100.00
											</div>
											<div class="cart-button">
												<button class="product-btn-add" type="button" onclick="cart.add('34');"><i class="fa fa-shopping-cart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('34');"><i class="fa fa-bar-chart"></i></button>
												<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('34');"><i class="fa fa-star"></i></button>
											</div>
										</div>

									</div>
								</div>-->

							</div>
						</div>
						<!--End feature-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content_bottom">
	</div>

	<!-- Scripts -->
	<script>
		(function($) {
			$(document).ready(function () {
				$('#camera_wrap_0').camera({
					navigation: true,
					playPause: false,
					thumbnails: false,
					navigationHover: false,
					barPosition: 'top',
					loader: false,
					time: 3000,
					transPeriod: 800,
					alignment: 'center',
					autoAdvance: true,
					mobileAutoAdvance: true,
					barDirection: 'leftToRight',
					barPosition: 'bottom',
					easing: 'easeInOutExpo',
					fx: 'simpleFade',
					height: '45.16653127538587%',
					minHeight: '300px',
					hover: true,
					pagination: false,
					loaderColor: '#1f1f1f',
					loaderBgColor: 'transparent',
					loaderOpacity: 1,
					loaderPadding: 0,
					loaderStroke: 3,
				});
			});
		})(jQuery);;
	</script>

