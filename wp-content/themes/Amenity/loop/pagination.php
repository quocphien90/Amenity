<?php
/**
 * Pagination - Show numbered pagination for catalog pages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/pagination.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;

if ( $wp_query->max_num_pages <= 1 ) {
	return;
}

?>
<?php
	$page_array = paginate_links( apply_filters( 'woocommerce_pagination_args', array(
		'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
		'format'       => '',
		'add_args'     => false,
		'current'      => max( 1, get_query_var( 'paged' ) ),
		'total'        => $wp_query->max_num_pages,
		'prev_text'    => '&lt;',
		'next_text'    => '&gt;',
		'type'         => 'array',
		'end_size'     => 3,
		'mid_size'     => 3,
	) ) );
	if ( is_array( $page_array ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var( 'paged' );

		$output .=  '<ul class="pagination">';
		foreach ( $page_array as $page ) {
			$output .= "<li>$page</li>";
		}
		$output .= '</ul>';

		// Create an instance of DOMDocument
		$dom = new \DOMDocument();

		// Populate $dom with $output, making sure to handle UTF-8, otherwise
		// problems will occur with UTF-8 characters.
		$dom->loadHTML( mb_convert_encoding( $output, 'HTML-ENTITIES', 'UTF-8' ) );

		// Create an instance of DOMXpath and all elements with the class 'page-numbers'
		$xpath = new \DOMXpath( $dom );

		// http://stackoverflow.com/a/26126336/3059883
		$page_numbers = $xpath->query( "//*[contains(concat(' ', normalize-space(@class), ' '), ' page-numbers ')]" );

		// Iterate over the $page_numbers node...
		foreach ( $page_numbers as $page_numbers_item ) {

			// Add class="mynewclass" to the <li> when its child contains the current item.
			$page_numbers_item_classes = explode( ' ', $page_numbers_item->attributes->item(0)->value );
			if ( in_array( 'current', $page_numbers_item_classes ) ) {
				$list_item_attr_class = $dom->createAttribute( 'class' );
				$list_item_attr_class->value = 'mynewclass';
				$page_numbers_item->parentNode->appendChild( $list_item_attr_class );
			}

			// Replace the class 'current' with 'active'
			$page_numbers_item->attributes->item(0)->value = str_replace(
				'current',
				'active',
				$page_numbers_item->attributes->item(0)->value );

			// Replace the class 'page-numbers' with 'page-link'
			$page_numbers_item->attributes->item(0)->value = str_replace(
				'page-numbers',
				'page-link',
				$page_numbers_item->attributes->item(0)->value );
		}

		// Save the updated HTML and output it.
		$output = $dom->saveHTML();
	}

	echo $output;
?>
