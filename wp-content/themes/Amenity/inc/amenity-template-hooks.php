<?php
/**
 * Amenity hooks
 *
 * @package amenity
 */

/**
 * General
 *
 * @see  amenity_header_widget_region()
 * @see  amenity_get_sidebar()
 */
add_action( 'amenity_before_content', 'amenity_header_widget_region', 10 );
add_action( 'amenity_sidebar',        'amenity_get_sidebar',          10 );

/**
 * Header
 *
 * @see  amenity_skip_links()
 * @see  amenity_secondary_navigation()
 * @see  amenity_site_branding()
 * @see  amenity_primary_navigation()
 */
/*add_action( 'amenity_header', 'amenity_skip_links',                       0 );
add_action( 'amenity_header', 'amenity_site_branding',                    20 );
add_action( 'amenity_header', 'amenity_secondary_navigation',             30 );
add_action( 'amenity_header', 'amenity_primary_navigation_wrapper',       42 );*/
add_action( 'amenity_header', 'amenity_primary_navigation',               50 );
//add_action( 'amenity_header', 'amenity_primary_navigation_wrapper_close', 68 );

/**
 * Footer
 *
 * @see  amenity_footer_widgets()
 * @see  amenity_credit()
 */
add_action( 'amenity_footer', 'amenity_footer_navigation', 0 );
//add_action( 'amenity_footer', 'amenity_footer_widgets', 10 );
//add_action( 'amenity_footer', 'amenity_credit',         20 );

/**
 * Homepage
 *
 * @see  amenity_homepage_content()
 * @see  amenity_product_categories()
 * @see  amenity_recent_products()
 * @see  amenity_featured_products()
 * @see  amenity_popular_products()
 * @see  amenity_on_sale_products()
 * @see  amenity_best_selling_products()
 */
add_action( 'homepage', 'amenity_homepage_content',      10 );
add_action( 'homepage_content', 'amenity_recent_products',   10,  1);


/**
 * Posts
 *
 * @see  amenity_post_header()
 * @see  amenity_post_meta()
 * @see  amenity_post_content()
 * @see  amenity_init_structured_data()
 * @see  amenity_paging_nav()
 * @see  amenity_single_post_header()
 * @see  amenity_post_nav()
 * @see  amenity_display_comments()
 */
add_action( 'amenity_loop_post',           'amenity_post_header',          10 );
add_action( 'amenity_loop_post',           'amenity_post_meta',            20 );
add_action( 'amenity_loop_post',           'amenity_post_content',         30 );
add_action( 'amenity_loop_post',           'amenity_init_structured_data', 40 );
add_action( 'amenity_loop_after',          'amenity_paging_nav',           10 );
add_action( 'amenity_single_post',         'amenity_post_header',          10 );
add_action( 'amenity_single_post',         'amenity_post_meta',            20 );
add_action( 'amenity_single_post',         'amenity_post_content',         30 );
add_action( 'amenity_single_post',         'amenity_init_structured_data', 40 );
add_action( 'amenity_single_post_bottom',  'amenity_post_nav',             10 );
add_action( 'amenity_single_post_bottom',  'amenity_display_comments',     20 );
add_action( 'amenity_post_content_before', 'amenity_post_thumbnail',       10 );

/**
 * Pages
 *
 * @see  amenity_page_header()
 * @see  amenity_page_content()
 * @see  amenity_init_structured_data()
 * @see  amenity_display_comments()
 */
/*add_action( 'amenity_page',       'amenity_page_header',          10 );
add_action( 'amenity_page',       'amenity_page_content',         20 );
add_action( 'amenity_page',       'amenity_init_structured_data', 30 );
add_action( 'amenity_homepage',       'amenity_init_structured_data', 30 );*/

add_action( 'amenity_products_page',       'amenity_get_products_by_category',      10, 1 );
