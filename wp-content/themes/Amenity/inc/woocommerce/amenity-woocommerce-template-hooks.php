<?php
/**
 * Storefront WooCommerce hooks
 *
 * @package amenity
 */

/**
 * Styles
 *
 * @see  amenity_woocommerce_scripts()
 */

/**
 * Layout
 *
 * @see  amenity_before_content()
 * @see  amenity_after_content()
 * @see  woocommerce_breadcrumb()
 * @see  amenity_shop_messages()
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',                   20, 0 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper',       10 );
remove_action( 'woocommerce_after_main_content',  'woocommerce_output_content_wrapper_end',   10 );
remove_action( 'woocommerce_sidebar',             'woocommerce_get_sidebar',                  10 );
remove_action( 'woocommerce_after_shop_loop',     'amenity_woocommerce_pagination',                   10, 1);
remove_action( 'woocommerce_before_shop_loop',    'woocommerce_result_count',                 20 );
remove_action( 'woocommerce_before_shop_loop',    'woocommerce_catalog_ordering',             30 );
//add_action( 'woocommerce_before_main_content',    'amenity_before_content',                10 );
//add_action( 'woocommerce_after_main_content',     'amenity_after_content',                 10 );
add_action( 'amenity_content_top',             'amenity_shop_messages',                 15 );
add_action( 'amenity_content_top',             'woocommerce_breadcrumb',                   10 );

add_action( 'woocommerce_after_shop_loop',        'amenity_sorting_wrapper',               9 );
add_action( 'woocommerce_after_shop_loop',        'woocommerce_catalog_ordering',             10 );
add_action( 'woocommerce_after_shop_loop',        'woocommerce_result_count',                 20 );
add_action( 'woocommerce_after_shop_loop',        'woocommerce_pagination',                   30 );
//add_action( 'woocommerce_after_shop_loop',        'amenity_sorting_wrapper_close',         31 );
add_action( 'woocommerce_after_shop_loop',        'amenity_product_columns_wrapper_close',    40 );

//add_filter( 'loop_shop_columns',                  'amenity_loop_columns' );

//add_action( 'woocommerce_before_shop_loop',       'amenity_sorting_wrapper',               9 );
add_action( 'woocommerce_before_shop_loop_order_by',       'amenity_woocommerce_catalog_ordering',             10 );
add_action( 'woocommerce_before_shop_loop_result_count',     'amenity_woocommerce_result_count',        20, 1 );
add_action( 'woocommerce_before_shop_loop_pagination',       'amenity_woocommerce_pagination',        20, 1 );

//add_action( 'amenity_footer',                  'amenity_handheld_footer_bar',           999 );

/**
 * Products
 *
 * @see  amenity_upsell_display()
 */
/*remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display',               15 );
add_action( 'woocommerce_after_single_product_summary',    'amenity_upsell_display',                15 );
remove_action( 'woocommerce_before_shop_loop_item_title',  'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_after_shop_loop_item_title',      'woocommerce_show_product_loop_sale_flash', 6 );*/

/**
 * Header
 *
 * @see  amenity_product_search()
 * @see  amenity_header_cart()
 */
//add_action( 'amenity_header', 'amenity_product_search', 40 );
add_action( 'amenity_header_cart', 'amenity_header_cart',    60 );

/**
 * Structured Data
 *
 * @see amenity_woocommerce_init_structured_data()
 */
if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.7', '<' ) ) {
	add_action( 'woocommerce_before_shop_loop_item', 'amenity_woocommerce_init_structured_data' );
}

if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.3', '>=' ) ) {
	add_filter( 'woocommerce_add_to_cart_fragments', 'amenity_cart_link_fragment' );
} else {
	add_filter( 'add_to_cart_fragments', 'amenity_cart_link_fragment' );
}
