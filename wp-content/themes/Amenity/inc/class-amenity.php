<?php
/**
 * Amenity Class
 *
 * @author   WooThemes
 * @since    1.0.0
 * @package  Amenity
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Amenity' ) ) :

	/**
	 * The main Amenity class
	 */
	class Amenity {

		private static $structured_data;

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'after_setup_theme',          array( $this, 'setup' ) );
			add_action( 'widgets_init',               array( $this, 'widgets_init' ) );
			add_action( 'wp_enqueue_scripts',         array( $this, 'scripts' ),       10 );
			add_action( 'wp_enqueue_scripts',         array( $this, 'child_scripts' ), 30 ); // After WooCommerce.
			add_filter( 'body_class',                 array( $this, 'body_classes' ) );
			add_filter( 'wp_page_menu_args',          array( $this, 'page_menu_args' ) );
			add_filter( 'navigation_markup_template', array( $this, 'navigation_markup_template' ) );
			add_action( 'enqueue_embed_scripts',      array( $this, 'print_embed_styles' ) );
			add_action( 'wp_footer',                  array( $this, 'get_structured_data' ) );
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {
			/*
			 * Load Localisation files.
			 *
			 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
			 */

			// Loads wp-content/languages/themes/amenity-it_IT.mo.
			load_theme_textdomain( 'amenity', trailingslashit( WP_LANG_DIR ) . 'themes/' );

			// Loads wp-content/themes/child-theme-name/languages/it_IT.mo.
			load_theme_textdomain( 'amenity', get_stylesheet_directory() . '/languages' );

			// Loads wp-content/themes/amenity/languages/it_IT.mo.
			load_theme_textdomain( 'amenity', get_template_directory() . '/languages' );

			/**
			 * Add default posts and comments RSS feed links to head.
			 */
			add_theme_support( 'automatic-feed-links' );

			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
			 */
			add_theme_support( 'post-thumbnails' );

			/**
			 * Enable support for site logo
			 */
			add_theme_support( 'custom-logo', array(
				'height'      => 110,
				'width'       => 470,
				'flex-width'  => true,
			) );

			// This theme uses wp_nav_menu() in two locations.
			register_nav_menus( array(
				'primary'   => __( 'Primary Menu', 'amenity' ),
				'secondary' => __( 'Secondary Menu', 'amenity' ),
				'handheld'  => __( 'Handheld Menu', 'amenity' ),
			) );

			/*
			 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'widgets',
			) );

			// Setup the WordPress core custom background feature.
			add_theme_support( 'custom-background', apply_filters( 'amenity_custom_background_args', array(
				'default-color' => apply_filters( 'amenity_default_background_color', 'ffffff' ),
				'default-image' => '',
			) ) );

			/**
			 *  Add support for the Site Logo plugin and the site logo functionality in JetPack
			 *  https://github.com/automattic/site-logo
			 *  http://jetpack.me/
			 */
			add_theme_support( 'site-logo', array( 'size' => 'full' ) );

			// Declare WooCommerce support.
			add_theme_support( 'woocommerce' );
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );

			// Declare support for title theme feature.
			add_theme_support( 'title-tag' );

			// Declare support for selective refreshing of widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );
		}

		/**
		 * Register widget area.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		public function widgets_init() {
			$sidebar_args['sidebar'] = array(
				'name'          => __( 'Sidebar', 'amenity' ),
				'id'            => 'sidebar-1',
				'description'   => ''
			);

			$sidebar_args['header'] = array(
				'name'        => __( 'Below Header', 'amenity' ),
				'id'          => 'header-1',
				'description' => __( 'Widgets added to this region will appear beneath the header and above the main content.', 'amenity' ),
			);

			$rows    = intval( apply_filters( 'amenity_footer_widget_rows', 1 ) );
			$regions = intval( apply_filters( 'amenity_footer_widget_columns', 4 ) );

			for ( $row = 1; $row <= $rows; $row++ ) {
				for ( $region = 1; $region <= $regions; $region++ ) {
					$footer_n = $region + $regions * ( $row - 1 ); // Defines footer sidebar ID.
					$footer   = sprintf( 'footer_%d', $footer_n );

					if ( 1 == $rows ) {
						$footer_region_name = sprintf( __( 'Footer Column %1$d', 'amenity' ), $region );
						$footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of the footer.', 'amenity' ), $region );
					} else {
						$footer_region_name = sprintf( __( 'Footer Row %1$d - Column %2$d', 'amenity' ), $row, $region );
						$footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of footer row %2$d.', 'amenity' ), $region, $row );
					}

					$sidebar_args[ $footer ] = array(
						'name'        => $footer_region_name,
						'id'          => sprintf( 'footer-%d', $footer_n ),
						'description' => $footer_region_description,
					);
				}
			}

			foreach ( $sidebar_args as $sidebar => $args ) {
				$widget_tags = array(
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<span class="gamma widget-title">',
					'after_title'   => '</span>',
				);

				/**
				 * Dynamically generated filter hooks. Allow changing widget wrapper and title tags. See the list below.
				 *
				 * 'amenity_header_widget_tags'
				 * 'amenity_sidebar_widget_tags'
				 *
				 * 'amenity_footer_1_widget_tags'
				 * 'amenity_footer_2_widget_tags'
				 * 'amenity_footer_3_widget_tags'
				 * 'amenity_footer_4_widget_tags'
				 */
				$filter_hook = sprintf( 'amenity_%s_widget_tags', $sidebar );
				$widget_tags = apply_filters( $filter_hook, $widget_tags );

				if ( is_array( $widget_tags ) ) {
					register_sidebar( $args + $widget_tags );
				}
			}
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function scripts() {
			global $amenity_version;
			$amenity_version = time();
			/**
			 * Styles
			 */
			wp_enqueue_style( 'amenity-style', get_template_directory_uri() . '/assets/content/theme/stylesheet/stylesheet.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-bootstrap', get_template_directory_uri() . '/assets/content/js/bootstrap/css/bootstrap.min.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-font-awesome', get_template_directory_uri() . '/assets/content/js/font-awesome/css/font-awesome.min.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-owl-carousel', get_template_directory_uri() . '/assets/content/js/jquery/owl-carousel/owl.carousel.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-magnificent', get_template_directory_uri() . '/assets/content/theme/stylesheet/magnificent.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-bxslider', get_template_directory_uri() . '/assets/content/theme/js/jquery.bxslider/jquery.bxslider.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-photoswipe', get_template_directory_uri() . '/assets/content/theme/stylesheet/photoswipe.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-fancybox', get_template_directory_uri() . '/assets/content/theme/js/fancybox/jquery.fancybox.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-tm_category_menu', get_template_directory_uri() . '/assets/content/js/tmcategory/tm_category_menu.css', '', $amenity_version );
			wp_enqueue_style( 'amenity-transitions', get_template_directory_uri() . '/assets/content/js/jquery/owl-carousel/owl.transitions.css"', '', $amenity_version );
			wp_enqueue_style( 'amenity-camera', get_template_directory_uri() . '/assets/content/js/camera/css/camera.css"', '', $amenity_version );



			wp_enqueue_style( 'amenity-icons', get_template_directory_uri() . '/assets/sass/base/icons.css', '', $amenity_version );

			/**
			 * Fonts
			 */
			$google_fonts = apply_filters( 'amenity_google_font_families', array(
				'source-sans-pro' => 'Source+Sans+Pro:400,300,300italic,400italic,600,700,900',
			) );

			$query_args = array(
				'family' => implode( '|', $google_fonts ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);

			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

			wp_enqueue_style( 'amenity-fonts', $fonts_url, array(), null );

			/**
			 * Scripts
			 */
			//wp_enqueue_script( 'amenity-navigation-js', get_template_directory_uri() . '/assets/js/navigation.min.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-common-js', get_template_directory_uri() . '/assets/content/theme/js/common.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-bootstrap-min-js', get_template_directory_uri() . '/assets/content/js/bootstrap/js/bootstrap.min.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-owl-carousel-min-js', get_template_directory_uri() . '/assets/content/js/jquery/owl-carousel/owl.carousel.min.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-camera-js', get_template_directory_uri() . '/assets/content/js/camera/camera.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-device-js', get_template_directory_uri() . '/assets/content/theme/js/device.min.js', array( 'jquery' ), $amenity_version, true );

			wp_enqueue_script( 'amenity-fancybox-js', get_template_directory_uri() . '/assets/content/theme/js/fancybox/jquery.fancybox.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-unveil-js', get_template_directory_uri() . '/assets/content/theme/js/jquery.unveil.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-superfish-js', get_template_directory_uri() . '/assets/content/theme/js/superfish.js',array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery.equalheights-js', get_template_directory_uri() . '/assets/content/theme/js/jquery.equalheights.js',array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery.scrollmagic.min-js', get_template_directory_uri() . '/assets/content/theme/js/greensock/jquery.scrollmagic.min.js',array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery.gsap.min-js', get_template_directory_uri() . '/assets/content/theme/js/greensock/jquery.gsap.min.js',array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-TimelineMax-js', get_template_directory_uri() . '/assets/content/theme/js/greensock/TimelineMax.min.js',array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-TweenMax.min-js', get_template_directory_uri() . '/assets/content/theme/js/greensock/TweenMax.min.js',array( 'jquery' ), $amenity_version, true );

			wp_enqueue_script( 'amenity-jquery-easing-js', get_template_directory_uri() . '/assets/content/theme/js/jquery.easing.1.3.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-ui-totop-js', get_template_directory_uri() . '/assets/content/theme/js/jquery.ui.totop.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-tmstickup-js', get_template_directory_uri() . '/assets/content/theme/js/tmstickup.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-photo-swipe-klass-js', get_template_directory_uri() . '/assets/content/theme/js/photo-swipe/klass.min.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-photo-swipe-photoswipe-js', get_template_directory_uri() . '/assets/content/theme/js/photo-swipe/code.photoswipe-3.0.5.min.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-photo-swipe-jquery-code-js', get_template_directory_uri() . '/assets/content/theme/js/photo-swipe/code.photoswipe.jquery-3.0.5.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-bxslider-js', get_template_directory_uri() . '/assets/content/theme/js/jquery.bxslider/jquery.bxslider.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-magnificent-js', get_template_directory_uri() . '/assets/content/theme/js/magnificent/magnificent.js', array( 'jquery', 'bridget' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-bridget-js', get_template_directory_uri() . '/assets/content/theme/js/magnificent/jquery.bridget.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-jquery-ba-throttle-debounce-js', get_template_directory_uri() . '/assets/content/theme/js/magnificent/jquery.ba-throttle-debounce.js', array( 'jquery' ), $amenity_version, true );
			wp_enqueue_script( 'amenity-script-main-js', get_template_directory_uri() . '/assets/content/theme/js/script.js',array( 'jquery'), $amenity_version, true );
				/*array( 'jquery',
					'amenity-jquery-easing-js',
					'amenity-jquery-ui-totop-js',
					'amenity-jquery-tmstickup-js',
					'amenity-jquery-ba-throttle-debounce-js',
					'amenity-jquery-bridget-js',
					'amenity-jquery-magnificent-js',
					'amenity-jquery-bxslider-js',
					'amenity-photo-swipe-klass-js',
					'amenity-photo-swipe-jquery-code-js',
					'amenity-photo-swipe-photoswipe-js',
					'amenity-jquery.gsap.min-js',
					'amenity-TimelineMax-js',
					'amenity-TweenMax.min-js',
					'amenity-jquery.scrollmagic.min-js',
					'amenity-jquery.equalheights-js',
					'amenity-unveil-js'), $amenity_version, true );*/


			if ( is_page_template( 'template-homepage.php' ) ) {
				//wp_enqueue_script( 'amenity-rgbaster', get_template_directory_uri() . '/assets/js/vendor/rgbaster.min.js', array( 'jquery' ), '1.1.0', true );
				//wp_enqueue_script( 'amenity-homepage', get_template_directory_uri() . '/assets/js/homepage.min.js', array( 'jquery' ), '20120206', true );
			}

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				//wp_enqueue_script( 'comment-reply' );
			}
		}

		/**
		 * Enqueue child theme stylesheet.
		 * A separate function is required as the child theme css needs to be enqueued _after_ the parent theme
		 * primary css and the separate WooCommerce css.
		 *
		 * @since  1.5.3
		 */
		public function child_scripts() {
			if ( is_child_theme() ) {
				wp_enqueue_style( 'amenity-child-style', get_stylesheet_uri(), '' );
			}
		}

		/**
		 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
		 *
		 * @param array $args Configuration arguments.
		 * @return array
		 */
		public function page_menu_args( $args ) {
			$args['show_home'] = true;
			return $args;
		}

		/**
		 * Adds custom classes to the array of body classes.
		 *
		 * @param array $classes Classes for the body element.
		 * @return array
		 */
		public function body_classes( $classes ) {
			// Adds a class of group-blog to blogs with more than 1 published author.
			if ( is_multi_author() ) {
				$classes[] = 'group-blog';
			}

			if ( ! function_exists( 'woocommerce_breadcrumb' ) ) {
				$classes[]	= 'no-wc-breadcrumb';
			}

			/**
			 * What is this?!
			 * Take the blue pill, close this file and forget you saw the following code.
			 * Or take the red pill, filter amenity_make_me_cute and see how deep the rabbit hole goes...
			 */
			$cute = apply_filters( 'amenity_make_me_cute', false );

			if ( true === $cute ) {
				$classes[] = 'amenity-cute';
			}

			// If our main sidebar doesn't contain widgets, adjust the layout to be full-width.
			if ( ! is_active_sidebar( 'sidebar-1' ) ) {
				$classes[] = 'amenity-full-width-content';
			}

			// Add class when using homepage template + featured image
			if ( is_page_template( 'template-homepage.php' ) && has_post_thumbnail() ) {
				$classes[] = 'has-post-thumbnail';
			}

			return $classes;
		}

		/**
		 * Custom navigation markup template hooked into `navigation_markup_template` filter hook.
		 */
		public function navigation_markup_template() {
			$template  = '<nav id="post-navigation" class="navigation %1$s" role="navigation" aria-label="Post Navigation">';
			$template .= '<span class="screen-reader-text">%2$s</span>';
			$template .= '<div class="nav-links">%3$s</div>';
			$template .= '</nav>';

			return apply_filters( 'amenity_navigation_markup_template', $template );
		}

		/**
		 * Add styles for embeds
		 */
		public function print_embed_styles() {
			wp_enqueue_style( 'source-sans-pro', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,700,900' );
			$accent_color     = get_theme_mod( 'amenity_accent_color' );
			$background_color = amenity_get_content_background_color();
			?>
			<style type="text/css">
				.wp-embed {
					padding: 2.618em !important;
					border: 0 !important;
					border-radius: 3px !important;
					font-family: "Source Sans Pro", "Open Sans", sans-serif !important;
					-webkit-font-smoothing: antialiased;
					background-color: <?php echo esc_html( amenity_adjust_color_brightness( $background_color, -7 ) ); ?> !important;
				}

				.wp-embed .wp-embed-featured-image {
					margin-bottom: 2.618em;
				}

				.wp-embed .wp-embed-featured-image img,
				.wp-embed .wp-embed-featured-image.square {
					min-width: 100%;
					margin-bottom: .618em;
				}

				a.wc-embed-button {
					padding: .857em 1.387em !important;
					font-weight: 600;
					background-color: <?php echo esc_attr( $accent_color ); ?>;
					color: #fff !important;
					border: 0 !important;
					line-height: 1;
					border-radius: 0 !important;
					box-shadow:
						inset 0 -1px 0 rgba(#000,.3);
				}

				a.wc-embed-button + a.wc-embed-button {
					background-color: #60646c;
				}
			</style>
			<?php
		}

		/**
		 * Sets `self::structured_data`.
		 *
		 * @param array $json
		 */
		public static function set_structured_data( $json ) {
			if ( ! is_array( $json ) ) {
				return;
			}

			self::$structured_data[] = $json;
		}

		/**
		 * Outputs structured data.
		 *
		 * Hooked into `wp_footer` action hook.
		 */
		public function get_structured_data() {
			if ( ! self::$structured_data ) {
				return;
			}

			$structured_data['@context'] = 'http://schema.org/';

			if ( count( self::$structured_data ) > 1 ) {
				$structured_data['@graph'] = self::$structured_data;
			} else {
				$structured_data = $structured_data + self::$structured_data[0];
			}

			echo '<script type="application/ld+json">' . wp_json_encode( $this->sanitize_structured_data( $structured_data ) ) . '</script>';
		}

		/**
		 * Sanitizes structured data.
		 *
		 * @param  array $data
		 * @return array
		 */
		public function sanitize_structured_data( $data ) {
			$sanitized = array();

			foreach ( $data as $key => $value ) {
				if ( is_array( $value ) ) {
					$sanitized_value = $this->sanitize_structured_data( $value );
				} else {
					$sanitized_value = sanitize_text_field( $value );
				}

				$sanitized[ sanitize_text_field( $key ) ] = $sanitized_value;
			}

			return $sanitized;
		}
	}
endif;

return new Amenity();
