<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Slider')
    ->show_on_post_type('page')
    ->show_on_page(27)
    ->add_fields(array(
        Field::make('complex', 'homepage_slide')->add_fields(array(
            Field::make('text', 'slide_link', 'Link'),
            Field::make('image', 'slide_photo', 'Photo')->set_value_type('url'),
        ))
));
Container::make('post_meta', 'Featured Category')
    ->show_on_post_type('page')
    ->show_on_page(27)
    ->add_fields(array(
        Field::make('complex', 'homepage_featured_category')->add_fields(array(
            Field::make('text', 'homepage_category_title', 'Category Title'),
            Field::make('image', 'homepage_category_photo', 'Category Thumb')->set_value_type('url'),
        ))
));