<?php
/**
 * Storefront template functions.
 *
 * @package amenity
 */

if ( ! function_exists( 'amenity_display_comments' ) ) {
	/**
	 * Storefront display comments
	 *
	 * @since  1.0.0
	 */
	function amenity_display_comments() {
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || '0' != get_comments_number() ) :
			comments_template();
		endif;
	}
}

if ( ! function_exists( 'amenity_comment' ) ) {
	/**
	 * Storefront comment template
	 *
	 * @param array $comment the comment array.
	 * @param array $args the comment args.
	 * @param int   $depth the comment depth.
	 * @since 1.0.0
	 */
	function amenity_comment( $comment, $args, $depth ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo esc_attr( $tag ); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-body">
		<div class="comment-meta commentmetadata">
			<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 128 ); ?>
			<?php printf( wp_kses_post( '<cite class="fn">%s</cite>', 'amenity' ), get_comment_author_link() ); ?>
			</div>
			<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_attr_e( 'Your comment is awaiting moderation.', 'amenity' ); ?></em>
				<br />
			<?php endif; ?>

			<a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>" class="comment-date">
				<?php echo '<time datetime="' . get_comment_date( 'c' ) . '">' . get_comment_date() . '</time>'; ?>
			</a>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-content">
		<?php endif; ?>
		<div class="comment-text">
		<?php comment_text(); ?>
		</div>
		<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		<?php edit_comment_link( __( 'Edit', 'amenity' ), '  ', '' ); ?>
		</div>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
	<?php
	}
}


if ( ! function_exists( 'amenity_footer_navigation' ) ) {
	/**
	 * Display the footer regions.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_footer_navigation()
	{
		?>
		<footer>
			<div class="footer">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>Information</h5>
							<ul class="list-unstyled">
								<li><a href="#0">About Us</a></li>
								<li><a href="#0">Delivery Information</a></li>
								<li><a href="#0">Privacy Policy</a></li>
								<li><a href="#0">Terms &amp; Conditions</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>Customer Service</h5>
							<ul class="list-unstyled">
								<li><a href="#0">Contact Us</a></li>
								<li><a href="#0">Returns</a></li>
								<li><a href="#0">Site Map</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>Extras</h5>
							<ul class="list-unstyled">
								<li><a href="#0">Brands</a></li>
								<li><a href="#0">Gift Vouchers</a></li>
								<li><a href="#0">Affiliates</a></li>
								<li><a href="#0">Specials</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>My Account</h5>
							<ul class="list-unstyled">
								<li><a href="#0">My Account</a></li>
								<li><a href="#0">Order History</a></li>
								<li><a href="#0">Wish List</a></li>
								<li><a href="#0">Newsletter</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>Follow us</h5>
							<ul class="social-list list-unstyled">
								<li>
									<a href='//www.facebook.com'>
										<!-- <i class="fa fa-facebook"></i> -->
										<span>Facebook</span>
									</a>
								</li>

								<li>
									<a href='//www.twitter.com'>
										<!-- <i class="fa fa-twitter"></i> -->
										<span>Twitter</span>
									</a>
								</li>

								<li>
									<a href='//wikipedia.org/wiki/RSS'>
										<!-- <i class="fa fa-youtube"></i> -->
										<span>RSS</span>
									</a>
								</li>

								<li>
									<a href='//www.youtube.com'>
										<!-- <i class="fa fa-youtube"></i> -->
										<span>Youtube</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 col-sm-3">
						<div class="footer_box">
							<h5>Contact Us</h5>
							<address class="fa fa-phone">
								<a href="callto:800-2345-6789">800-2345-6789</a>
								<a href="callto:800-2345-6790">800-2345-6790</a>
							</address>
						</div>
					</div>
				</div>

			</div>
			<div class="copyright">
				<div class="container">
					Powered By <a href="https://www.opencart.com">OpenCart</a><br /> Flooring &copy; 2017<!-- [[%FOOTER_LINK]] -->
				</div>
			</div>
		</footer>
		<?php
	}
}

if ( ! function_exists( 'amenity_footer_widgets' ) ) {
	/**
	 * Display the footer widget regions.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_footer_widgets() {
		$rows    = intval( apply_filters( 'amenity_footer_widget_rows', 1 ) );
		$regions = intval( apply_filters( 'amenity_footer_widget_columns', 4 ) );

		for ( $row = 1; $row <= $rows; $row++ ) :

			// Defines the number of active columns in this footer row.
			for ( $region = $regions; 0 < $region; $region-- ) {
				if ( is_active_sidebar( 'footer-' . strval( $region + $regions * ( $row - 1 ) ) ) ) {
					$columns = $region;
					break;
				}
			}

			if ( isset( $columns ) ) : ?>
				<div class=<?php echo '"footer-widgets row-' . strval( $row ) . ' col-' . strval( $columns ) . ' fix"'; ?>><?php

					for ( $column = 1; $column <= $columns; $column++ ) :
						$footer_n = $column + $regions * ( $row - 1 );

						if ( is_active_sidebar( 'footer-' . strval( $footer_n ) ) ) : ?>

							<div class="block footer-widget-<?php echo strval( $column ); ?>">
								<?php dynamic_sidebar( 'footer-' . strval( $footer_n ) ); ?>
							</div><?php

						endif;
					endfor; ?>

				</div><!-- .footer-widgets.row-<?php echo strval( $row ); ?> --><?php

				unset( $columns );
			endif;
		endfor;
	}
}

if ( ! function_exists( 'amenity_credit' ) ) {
	/**
	 * Display the theme credit
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_credit() {
		?>
		<div class="site-info">
			<?php echo esc_html( apply_filters( 'amenity_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
			<?php if ( apply_filters( 'amenity_credit_link', true ) ) { ?>
			<br /> <?php printf( esc_attr__( '%1$s designed by %2$s.', 'amenity' ), 'Storefront', '<a href="http://www.woocommerce.com" title="WooCommerce - The Best eCommerce Platform for WordPress" rel="author">WooCommerce</a>' ); ?>
			<?php } ?>
		</div><!-- .site-info -->
		<?php
	}
}

if ( ! function_exists( 'amenity_header_widget_region' ) ) {
	/**
	 * Display header widget region
	 *
	 * @since  1.0.0
	 */
	function amenity_header_widget_region() {
		if ( is_active_sidebar( 'header-1' ) ) {
		?>
		<div class="header-widget-region" role="complementary">
			<div class="col-full">
				<?php dynamic_sidebar( 'header-1' ); ?>
			</div>
		</div>
		<?php
		}
	}
}

if ( ! function_exists( 'amenity_site_branding' ) ) {
	/**
	 * Site branding wrapper and display
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_site_branding() {
		?>
		<div class="site-branding">
			<?php amenity_site_title_or_logo(); ?>
		</div>
		<?php
	}
}

if ( ! function_exists( 'amenity_site_title_or_logo' ) ) {
	/**
	 * Display the site title or logo
	 *
	 * @since 2.1.0
	 * @param bool $echo Echo the string or return it.
	 * @return string
	 */
	function amenity_site_title_or_logo( $echo = true ) {
		if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
			$logo = get_custom_logo();
			$html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;
		} elseif ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
			// Copied from jetpack_the_site_logo() function.
			$logo    = site_logo()->logo;
			$logo_id = get_theme_mod( 'custom_logo' ); // Check for WP 4.5 Site Logo
			$logo_id = $logo_id ? $logo_id : $logo['id']; // Use WP Core logo if present, otherwise use Jetpack's.
			$size    = site_logo()->theme_size();
			$html    = sprintf( '<a href="%1$s" class="site-logo-link" rel="home" itemprop="url">%2$s</a>',
				esc_url( home_url( '/' ) ),
				wp_get_attachment_image(
					$logo_id,
					$size,
					false,
					array(
						'class'     => 'site-logo attachment-' . $size,
						'data-size' => $size,
						'itemprop'  => 'logo'
					)
				)
			);

			$html = apply_filters( 'jetpack_the_site_logo', $html, $logo, $size );
		} else {
			$tag = is_home() ? 'h1' : 'div';

			$html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) .'>';

			if ( '' !== get_bloginfo( 'description' ) ) {
				$html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
			}
		}

		if ( ! $echo ) {
			return $html;
		}

		echo $html;
	}
}

if ( ! function_exists( 'amenity_primary_navigation' ) ) {
	/**
	 * Display Primary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_primary_navigation() {
		?>
		<div class="bg-main">
			<nav id="top-links" class="nav">
				<ul class="list-inline">
					<li class="first">
						<a href="#0">
							<i class="fa fa-home"></i><span>Trang chủ</span>
						</a>
					</li>
					<li>
						<a href="#0" id="wishlist-total"
						   title="Wish List (0)">
							<i class="fa fa-heart"></i> <span>Wish List (0)</span>
						</a>
					</li>
					<!--<li class="dropdown">
						<a href="#0" title="My Account"
						   class="dropdown-toggle"
						   data-toggle="dropdown">
							<i class="fa fa-user"></i>
							<span>My Account</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-left">
							<li>
								<a href="#0">Register</a>
							</li>
							<li>
								<a href="#0">Login</a>
							</li>
						</ul>
					</li>-->
					<li>
						<a href="#0" title="Đồ dùng khách sạn">
							<i class="fa fa-shopping-cart"></i> <span>Đồ dùng khách sạn</span>
						</a>
					</li>
					<li>
						<a href="#0" title="Checkout">
							<i class="fa fa-share"></i> <span>Checkout</span>
						</a>
					</li>
				</ul>
			</nav>
			<!--<div id="search" class="search">
				<input type="text" name="search" value="" />
				<button type="button" class="button-search"></button>
			</div>-->
			<?php
				echo do_shortcode("[aws_search_form]");
			?>

		</div>
		<?php
	}
}

if ( ! function_exists( 'amenity_secondary_navigation' ) ) {
	/**
	 * Display Secondary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_secondary_navigation() {
	    if ( has_nav_menu( 'secondary' ) ) {
		    ?>
		    <nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'amenity' ); ?>">
			    <?php
				    wp_nav_menu(
					    array(
						    'theme_location'	=> 'secondary',
						    'fallback_cb'		=> '',
					    )
				    );
			    ?>
		    </nav><!-- #site-navigation -->
		    <?php
		}
	}
}

if ( ! function_exists( 'amenity_skip_links' ) ) {
	/**
	 * Skip links
	 *
	 * @since  1.4.1
	 * @return void
	 */
	function amenity_skip_links() {
		?>
		<a class="skip-link screen-reader-text" href="#site-navigation"><?php esc_attr_e( 'Skip to navigation', 'amenity' ); ?></a>
		<a class="skip-link screen-reader-text" href="#content"><?php esc_attr_e( 'Skip to content', 'amenity' ); ?></a>
		<?php
	}
}

if ( ! function_exists( 'amenity_homepage_header' ) ) {
	/**
	 * Display the page header without the featured image
	 *
	 * @since 1.0.0
	 */
	function amenity_homepage_header() {
		edit_post_link( __( 'Edit this section', 'amenity' ), '', '', '', 'button amenity-hero__button-edit' );
		?>
		<header class="entry-header">
			<?php
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'amenity_page_header' ) ) {
	/**
	 * Display the page header
	 *
	 * @since 1.0.0
	 */
	function amenity_page_header() {
		?>
		<header class="entry-header">
			<?php
			amenity_post_thumbnail( 'full' );
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'amenity_page_content' ) ) {
	/**
	 * Display the post content
	 *
	 * @since 1.0.0
	 */
	function amenity_page_content() {
		?>
		<div class="entry-content">
			<?php //the_content(); ?>
			<?php
				/*wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'amenity' ),
					'after'  => '</div>',
				) );*/
			?>
		</div><!-- .entry-content
		<?php
	}
}

if ( ! function_exists( 'amenity_post_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function amenity_post_header() {
		?>
		<header class="entry-header">
		<?php
		if ( is_single() ) {
			amenity_posted_on();
			the_title( '<h1 class="entry-title">', '</h1>' );
		} else {
			if ( 'post' == get_post_type() ) {
				amenity_posted_on();
			}

			the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		}
		?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'amenity_post_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function amenity_post_content() {
		?>
		<div class="entry-content">
		<?php

		/**
		 * Functions hooked in to amenity_post_content_before action.
		 *
		 * @hooked amenity_post_thumbnail - 10
		 */
		do_action( 'amenity_post_content_before' );

		the_content(
			sprintf(
				__( 'Continue reading %s', 'amenity' ),
				'<span class="screen-reader-text">' . get_the_title() . '</span>'
			)
		);

		do_action( 'amenity_post_content_after' );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'amenity' ),
			'after'  => '</div>',
		) );
		?>
		</div><!-- .entry-content -->
		<?php
	}
}

if ( ! function_exists( 'amenity_post_meta' ) ) {
	/**
	 * Display the post meta
	 *
	 * @since 1.0.0
	 */
	function amenity_post_meta() {
		?>
		<aside class="entry-meta">
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search.

			?>
			<div class="author">
				<?php
					echo get_avatar( get_the_author_meta( 'ID' ), 128 );
					echo '<div class="label">' . esc_attr( __( 'Written by', 'amenity' ) ) . '</div>';
					the_author_posts_link();
				?>
			</div>
			<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'amenity' ) );

			if ( $categories_list ) : ?>
				<div class="cat-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Posted in', 'amenity' ) ) . '</div>';
					echo wp_kses_post( $categories_list );
					?>
				</div>
			<?php endif; // End if categories. ?>

			<?php
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'amenity' ) );

			if ( $tags_list ) : ?>
				<div class="tags-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Tagged', 'amenity' ) ) . '</div>';
					echo wp_kses_post( $tags_list );
					?>
				</div>
			<?php endif; // End if $tags_list. ?>

		<?php endif; // End if 'post' == get_post_type(). ?>

			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
				<div class="comments-link">
					<?php echo '<div class="label">' . esc_attr( __( 'Comments', 'amenity' ) ) . '</div>'; ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'amenity' ), __( '1 Comment', 'amenity' ), __( '% Comments', 'amenity' ) ); ?></span>
				</div>
			<?php endif; ?>
		</aside>
		<?php
	}
}

if ( ! function_exists( 'amenity_paging_nav' ) ) {
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function amenity_paging_nav() {
		global $wp_query;

		$args = array(
			'type' 	    => 'list',
			'next_text' => _x( 'Next', 'Next post', 'amenity' ),
			'prev_text' => _x( 'Previous', 'Previous post', 'amenity' ),
			);

		the_posts_pagination( $args );
	}
}

if ( ! function_exists( 'amenity_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function amenity_post_nav() {
		$args = array(
			'next_text' => '%title',
			'prev_text' => '%title',
			);
		the_post_navigation( $args );
	}
}

if ( ! function_exists( 'amenity_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function amenity_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			_x( 'Posted on %s', 'post date', 'amenity' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo wp_kses( apply_filters( 'amenity_single_post_posted_on_html', '<span class="posted-on">' . $posted_on . '</span>', $posted_on ), array(
			'span' => array(
				'class'  => array(),
			),
			'a'    => array(
				'href'  => array(),
				'title' => array(),
				'rel'   => array(),
			),
			'time' => array(
				'datetime' => array(),
				'class'    => array(),
			),
		) );
	}
}

if ( ! function_exists( 'amenity_product_categories' ) ) {
	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function amenity_product_categories( $args ) {

		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_product_categories_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'child_categories' 	=> 0,
				'orderby' 			=> 'name',
				'title'				=> __( 'Shop by Category', 'amenity' ),
			) );

			$shortcode_content = amenity_do_shortcode( 'product_categories', apply_filters( 'amenity_product_categories_shortcode_args', array(
				'number'  => intval( $args['limit'] ),
				'columns' => intval( $args['columns'] ),
				'orderby' => esc_attr( $args['orderby'] ),
				'parent'  => esc_attr( $args['child_categories'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns product categories
			 */
			if ( false !== strpos( $shortcode_content, 'product-category' ) ) {

				echo '<section class="amenity-product-section amenity-product-categories" aria-label="' . esc_attr__( 'Product Categories', 'amenity' ) . '">';

				do_action( 'amenity_homepage_before_product_categories' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'amenity_homepage_after_product_categories_title' );

				echo wp_kses_post( $shortcode_content );

				do_action( 'amenity_homepage_after_product_categories' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'amenity_recent_products' ) ) {
	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function amenity_recent_products( $args ) {
		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_recent_products_args', array(
				'limit' 			=> !(empty($args['limit'])) ? $args['limit'] : 4,
				'columns' 			=> 4,
			) );

			$shortcode_content = amenity_do_shortcode( 'recent_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			$atts = shortcode_atts( array(
				'per_page' => '9',
				'columns'  => '4',
				'orderby'  => 'date',
				'order'    => 'desc',
				'category' => '',  // Slugs
				'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
			), $args, 'recent_products' );

			$query_args = array(
				'post_type'           => 'product',
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page'      => $atts['per_page'],
				'orderby'             => $atts['orderby'],
				'order'               => $atts['order'],
			);

			global $woocommerce_loop;
			$loop_name = 'homepage_recent_products';
			$columns                     = absint( $atts['columns'] );
			$woocommerce_loop['columns'] = $columns;
			$query_args                  = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, $loop_name );
			$transient_name              = 'wc_loop' . substr( md5( json_encode( $query_args ) . $loop_name ), 28 ) . WC_Cache_Helper::get_transient_version( 'product_query' );
			$products                    = get_transient( $transient_name );

			if ( false === $products || ! is_a( $products, 'WP_Query' ) ) {
				$products = new WP_Query( $query_args );
				set_transient( $transient_name, $products, DAY_IN_SECONDS * 30 );
			}
			ob_start();

			if ( $products->have_posts() ) {
				?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>
					<?php wc_get_template_part( 'content', 'product' ); ?>
				<?php endwhile; // end of the loop. ?>


				<?php
			}
			woocommerce_reset_loop();
			wp_reset_postdata();
		}
	}
}

if ( ! function_exists( 'amenity_featured_products' ) ) {
	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function amenity_featured_products( $args ) {

		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_featured_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'orderby' => 'date',
				'order'   => 'desc',
				'title'   => __( 'We Recommend', 'amenity' ),
			) );

			$shortcode_content = amenity_do_shortcode( 'featured_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
				'orderby'  => esc_attr( $args['orderby'] ),
				'order'    => esc_attr( $args['order'] ),
			) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="amenity-product-section amenity-featured-products" aria-label="' . esc_attr__( 'Featured Products', 'amenity' ) . '">';

				do_action( 'amenity_homepage_before_featured_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'amenity_homepage_after_featured_products_title' );

				echo wp_kses_post( $shortcode_content );

				do_action( 'amenity_homepage_after_featured_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'amenity_popular_products' ) ) {
	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function amenity_popular_products( $args ) {

		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_popular_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'Fan Favorites', 'amenity' ),
			) );

			$shortcode_content = amenity_do_shortcode( 'top_rated_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="amenity-product-section amenity-popular-products" aria-label="' . esc_attr__( 'Popular Products', 'amenity' ) . '">';

				do_action( 'amenity_homepage_before_popular_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'amenity_homepage_after_popular_products_title' );

				echo wp_kses_post( $shortcode_content );

				do_action( 'amenity_homepage_after_popular_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'amenity_on_sale_products' ) ) {
	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @param array $args the product section args.
	 * @since  1.0.0
	 * @return void
	 */
	function amenity_on_sale_products( $args ) {

		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_on_sale_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'On Sale', 'amenity' ),
			) );

			$shortcode_content = amenity_do_shortcode( 'sale_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="amenity-product-section amenity-on-sale-products" aria-label="' . esc_attr__( 'On Sale Products', 'amenity' ) . '">';

				do_action( 'amenity_homepage_before_on_sale_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'amenity_homepage_after_on_sale_products_title' );

				echo wp_kses_post( $shortcode_content );

				do_action( 'amenity_homepage_after_on_sale_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'amenity_best_selling_products' ) ) {
	/**
	 * Display Best Selling Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since 2.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function amenity_best_selling_products( $args ) {
		if ( amenity_is_woocommerce_activated() ) {

			$args = apply_filters( 'amenity_best_selling_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'	  => esc_attr__( 'Best Sellers', 'amenity' ),
			) );

			$shortcode_content = amenity_do_shortcode( 'best_selling_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="amenity-product-section amenity-best-selling-products" aria-label="' . esc_attr__( 'Best Selling Products', 'amenity' ) . '">';

				do_action( 'amenity_homepage_before_best_selling_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'amenity_homepage_after_best_selling_products_title' );

				echo wp_kses_post( $shortcode_content );

				do_action( 'amenity_homepage_after_best_selling_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'amenity_homepage_content' ) ) {
	/**
	 * Display homepage content
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function amenity_homepage_content() {
		while ( have_posts() ) {
			the_post();

			get_template_part( 'content', 'homepage' );

		} // end of the loop.
	}
}


if ( ! function_exists( 'amenity_get_sidebar' ) ) {
	/**
	 * Display amenity sidebar
	 *
	 * @uses get_sidebar()
	 * @since 1.0.0
	 */
	function amenity_get_sidebar() {
		get_sidebar();
	}
}

if ( ! function_exists( 'amenity_post_thumbnail' ) ) {
	/**
	 * Display post thumbnail
	 *
	 * @var $size thumbnail size. thumbnail|medium|large|full|$custom
	 * @uses has_post_thumbnail()
	 * @uses the_post_thumbnail
	 * @param string $size the post thumbnail size.
	 * @since 1.5.0
	 */
	function amenity_post_thumbnail( $size = 'full' ) {
		if ( has_post_thumbnail() ) {
			the_post_thumbnail( $size );
		}
	}
}

if ( ! function_exists( 'amenity_primary_navigation_wrapper' ) ) {
	/**
	 * The primary navigation wrapper
	 */
	function amenity_primary_navigation_wrapper() {
		echo '<div class="amenity-primary-navigation">';
	}
}

if ( ! function_exists( 'amenity_primary_navigation_wrapper_close' ) ) {
	/**
	 * The primary navigation wrapper close
	 */
	function amenity_primary_navigation_wrapper_close() {
		echo '</div>';
	}
}

if ( ! function_exists( 'amenity_init_structured_data' ) ) {
	/**
	 * Generates structured data.
	 *
	 * Hooked into the following action hooks:
	 *
	 * - `amenity_loop_post`
	 * - `amenity_single_post`
	 * - `amenity_page`
	 *
	 * Applies `amenity_structured_data` filter hook for structured data customization :)
	 */
	function amenity_init_structured_data() {

		// Post's structured data.
		if ( is_home() || is_category() || is_date() || is_search() || is_single() && ( amenity_is_woocommerce_activated() && ! is_woocommerce() ) ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'normal' );
			$logo  = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' );

			$json['@type']            = 'BlogPosting';

			$json['mainEntityOfPage'] = array(
				'@type'                 => 'webpage',
				'@id'                   => get_the_permalink(),
			);

			$json['publisher']        = array(
				'@type'                 => 'organization',
				'name'                  => get_bloginfo( 'name' ),
				'logo'                  => array(
					'@type'               => 'ImageObject',
					'url'                 => $logo[0],
					'width'               => $logo[1],
					'height'              => $logo[2],
				),
			);

			$json['author']           = array(
				'@type'                 => 'person',
				'name'                  => get_the_author(),
			);

			if ( $image ) {
				$json['image']            = array(
					'@type'                 => 'ImageObject',
					'url'                   => $image[0],
					'width'                 => $image[1],
					'height'                => $image[2],
				);
			}

			$json['datePublished']    = get_post_time( 'c' );
			$json['dateModified']     = get_the_modified_date( 'c' );
			$json['name']             = get_the_title();
			$json['headline']         = $json['name'];
			$json['description']      = get_the_excerpt();

		// Page's structured data.
		} elseif ( is_page() ) {
			$json['@type']            = 'WebPage';
			$json['url']              = get_the_permalink();
			$json['name']             = get_the_title();
			$json['description']      = get_the_excerpt();
		}

		if ( isset( $json ) ) {
			Amenity::set_structured_data( apply_filters( 'amenity_structured_data', $json ) );
		}
	}
}
if ( ! function_exists( 'amenity_get_products_by_category' ) ) {
	function amenity_get_products_by_category($args) {
		if ( amenity_is_woocommerce_activated() ) {
			global $woocommerce_loop;
			$orderby                 = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
			$args = apply_filters( 'amenity_recent_products_args', array(
				'limit' 			=> !(empty($args['limit'])) ? $args['limit'] : 4,
				'category' 			=> $args['category'],
				'page'	  => $args['page'],
			) );



			$atts = shortcode_atts( array(
				'per_page' =>  $args['limit'],
				'orderby'  => 'date',
				'order'    => 'desc',
				'page'	  => $args['page'],
				'category' => $args['category'],
			), $args, 'recent_products' );

			$query_args = array(
				'post_type'           => 'product',
				'post_status'         => 'publish',
				'product_cat'		  => $atts['category'],
				'paged'	  			  => $atts['page'],
				'posts_per_page'      => $atts['per_page'],
				'orderby'             => 'date',
				'order'               => 'DESC',
			);

            switch($orderby)
            {
                case 'price':
                    $query_args['orderby'] = 'meta_value_num';
                    $query_args['meta_key'] = '_price';
                    $query_args['order'] = 'ASC';
                    break;
                case 'price-desc':
                    $query_args['orderby'] = 'meta_value_num';
                    $query_args['meta_key'] = '_price';
                    $query_args['order'] = 'DESC';
                    break;
                case 'date':
                default:
                    break;
            }

			$loop_name = 'categorypage_recent_products';
			$query_args                  = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, $loop_name );
			$transient_name              = 'wc_loop' . substr( md5( json_encode( $query_args ) . $loop_name ), 28 ) . WC_Cache_Helper::get_transient_version( 'product_query' );
			$products                    = get_transient( $transient_name );
			if ( false === $products || ! is_a( $products, 'WP_Query' ) ) {
				$products = new WP_Query( $query_args );
				set_transient( $transient_name, $products, DAY_IN_SECONDS * 30 );
			}
			ob_start();
			//echo '<pre>' . print_r($products,true). '</pre>';die;
			if ( $products->have_posts() ) {
				while ($products->have_posts()) : $products->the_post();
					wc_get_template_part('content', 'product_cat');
				endwhile; // end of the loop.
			}
			woocommerce_reset_loop();
			wp_reset_postdata();
		}
	}
}
if ( ! function_exists( 'amenity_woocommerce_result_count' ) ) {
	function amenity_woocommerce_result_count($args)
	{
		global $wp_query;
		$atts = shortcode_atts( array(
			'per_page' =>  $args['limit'],
			'page'	  => $args['page'],
			'category' => $args['category'],
			'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
		), $args, 'category_products' );



		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'product_cat'		  => $atts['category'],
			'paged'	  			  => $atts['page'],
			'posts_per_page'      => $atts['per_page'],
		);

		$wp_query = new WP_Query($query_args);
		ob_start();
		wc_get_template( 'loop/result-count.php' );

	}
}
if ( ! function_exists( 'amenity_woocommerce_pagination' ) ) {
	function amenity_woocommerce_pagination($args)
	{

		global $wp_query;
		$atts = shortcode_atts( array(
			'per_page' =>  $args['limit'],
			'page'	  => $args['page'],
			'category' => $args['category'],
		), $args, 'category_products' );



		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'paged'	  			  => $atts['page'],
			'product_cat'		  => $atts['category'],
			'posts_per_page'      => $atts['per_page'],
		);
		$wp_query = new WP_Query($query_args);
		ob_start();
		wc_get_template( 'loop/pagination.php' );

	}
}

if ( ! function_exists( 'amenity_woocommerce_catalog_ordering' ) ) {
	function amenity_woocommerce_catalog_ordering($args)
	{
		global $wp_query;

		if ( 1 === (int) $wp_query->found_posts || ! woocommerce_products_will_display() ) {
			return;
		}

		$orderby                 = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
		$show_default_orderby    = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
		$catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array(
			'menu_order' => __( 'Default sorting', 'woocommerce' ),
			'date'       => __( 'Sắp xếp theo sản phẩm mới', 'woocommerce' ),
			'price'      => __( 'Sắp xếp theo giá tăng dần', 'woocommerce' ),
			'price-desc' => __( 'Sắp xếp theo giá giảm dần', 'woocommerce' ),
		) );

		if ( ! $show_default_orderby ) {
			unset( $catalog_orderby_options['menu_order'] );
		}

		if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
			unset( $catalog_orderby_options['rating'] );
		}

		wc_get_template( 'loop/orderby.php', array( 'catalog_orderby_options' => $catalog_orderby_options, 'orderby' => $orderby, 'show_default_orderby' => $show_default_orderby ) );
	}
}

if ( ! function_exists( 'amenity_woocommerce_show_product_images' ) ) {

    /**
     * Output the product image before the single product summary.
     *
     * @subpackage	Product
     */
    function amenity_woocommerce_show_product_images() {

        global $post, $product;
        $columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
        $full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
        $image_title       = get_post_field( 'post_excerpt', $post_thumbnail_id );
        $placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
        $wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
            'woocommerce-product-gallery',
            'woocommerce-product-gallery--' . $placeholder,
            'woocommerce-product-gallery--columns-' . absint( $columns ),
            'images',
        ) );
        $attributes = array(
            'title'                   => $image_title,
            'data-src'                => $full_size_image[0],
            'data-large_image'        => $full_size_image[0],
            'data-large_image_width'  => $full_size_image[1],
            'data-large_image_height' => $full_size_image[2],
        );

        $attachment_ids = $product->get_gallery_image_ids();

        if ( $attachment_ids && has_post_thumbnail() ) {
            foreach ($attachment_ids as $attachment_id) {
                $full_size_image = wp_get_attachment_image_src($attachment_id, 'full');
                $thumbnail = wp_get_attachment_image_src($attachment_id, 'shop_thumbnail');
                $image_title = get_post_field('post_excerpt', $attachment_id);
            }
        }
    }
}