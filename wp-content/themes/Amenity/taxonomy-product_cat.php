<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Product Category
 *
 * @package amenity
 */
$page_size = 2;
$current_page = get_query_var("paged");
if ( is_product_taxonomy() ) {
    $term = get_queried_object();
    $term_name = $term->slug;
}

$args = array(
    'category' => $term_name,
    'limit' => $page_size,
    'page' => $current_page
);
//echo '<pre>' . print_r($wp_query,true). '</pre>';die;
get_header(); ?>


    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#0"><i class="fa fa-home"></i></a></li>
            <li><a href="#0">HARDWOOD</a></li>
        </ul>
        <div class="row">
            <?php echo do_action("amenity_sidebar") ?>

            <!-- Product list -->
            <div id="content" class="col-sm-9">
                <!-- Sub Categories -->
                <div class="product-filter clearfix">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="input-sort">Sort By:</label>
                        </div>
                        <div class="col-md-3">
                            <?php echo do_action("woocommerce_before_shop_loop_order_by") ?>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label" for="input-limit">Show:</label>
                        </div>
                        <div class="col-md-2">
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=6" selected="selected">6</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=25">25</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=50">50</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=75">75</option>
                                <option value="https://livedemo00.template-help.com/opencart_54855/index.php?route=product/category&amp;path=33&amp;limit=100">100</option>
                            </select>
                        </div>
                        <div class="col-md-3 text-right">
                            <div class="button-view">
                                <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                                <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-cat clearfix">

                    <div class="pull-left">
                        <?php do_action("woocommerce_before_shop_loop_pagination", $args); ?>
                    </div>
                    <div class="pull-left nam-page"> <?php do_action( "woocommerce_before_shop_loop_result_count", $args);?></div>
                </div>
                <div class="row">
                    <?php
                        do_action("amenity_products_page", $args);
                    ?>

                </div>
                <div class="nav-cat clearfix">
                    <div class="pull-left nam-page">
                        <?php do_action("woocommerce_before_shop_loop_pagination", $args); ?>
                    </div>
                    <div class="pull-left nam-page"><?php do_action( "woocommerce_before_shop_loop_result_count", $args);?></div>
                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
