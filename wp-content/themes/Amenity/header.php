<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <div class="swipe">
    <div class="swipe-menu">
        <ul>

            <li>
                <a href="#0" title="My Account">
                    <i class="fa fa-user"></i>
                    <span>My Account</span>
                </a>
            </li>
            <li>
                <a href="#0"><i class="fa fa-user"></i> Register</a>
            </li>
            <li>
                <a href="#0"><i class="fa fa-lock"></i>Login</a>
            </li>
            <li>
                <a href="#0" id="wishlist-total2" title="Wish List (0)">
                    <i class="fa fa-heart"></i> <span>Wish List (0)</span>
                </a>
            </li>
            <li>
                <a href="#0" title="Shopping Cart">
                    <i class="fa fa-shopping-cart"></i> <span>Shopping Cart</span>
                </a>
            </li>
            <li>
                <a href="#0" title="Checkout">
                    <i class="fa fa-share"></i>
                    <span>Checkout</span>
                </a>
            </li>
        </ul>
        <ul class="foot">
            <li>
                <a href="#0">About Us</a>
            </li>
            <li>
                <a href="#0">Delivery Information</a>
            </li>
            <li>
                <a href="#0">Privacy Policy</a>
            </li>
            <li>
                <a href="#0">Terms &amp; Conditions</a>
            </li>
        </ul>
        <ul class="foot foot-1">
            <li>
                <a href="#0">Contact Us</a>
            </li>
            <li>
                <a href="#0">Returns</a>
            </li>
            <li>
                <a href="#0">Site Map</a>
            </li>
        </ul>

        <ul class="foot foot-2">
            <li>
                <a href="#0">Brands</a>
            </li>
            <li>
                <a href="#0">Gift Vouchers</a>
            </li>
            <li>
                <a href="#0">Affiliates</a>
            </li>
            <li>
                <a href="#0">Specials</a>
            </li>
        </ul>
        <ul class="foot foot-3">
            <li>
                <a href="#0">Order History</a>
            </li>
            <li>
                <a href="#0">Newsletter</a>
            </li>
        </ul>
    </div>
</div>
    <div id="page">
        <div class="shadow"></div>
        <div class="toprow-1">
            <a class="swipe-control" href="#"><i class="fa fa-align-justify"></i></a>
        </div>
        <header class="header">
            <div class="container">
                <div id="logo" class="logo">
                    <a href="#0">
                        <img src="<?php echo get_template_directory_uri();?>/images/logo.png" title="Flooring"
                             alt="Flooring" class="img-responsive" />
                    </a>
                </div>
               <?php  do_action( 'amenity_header_cart' ); ?>
                <address class="fa fa-phone"><a href="callto:800-2345-6789">800-2345-6789</a></address>
            </div>
<?php
        /**
         * Functions hooked into amenity_header action
         *
         * @hooked amenity_skip_links                       - 0
         * @hooked amenity_social_icons                     - 10
         * @hooked amenity_site_branding                    - 20
         * @hooked amenity_secondary_navigation             - 30
         * @hooked amenity_product_search                   - 40
         * @hooked amenity_primary_navigation_wrapper       - 42
         * @hooked amenity_primary_navigation               - 50
         * @hooked amenity_header_cart                      - 60
         * @hooked amenity_primary_navigation_wrapper_close - 68
         */
        do_action( 'amenity_header' ); ?>
        </header>