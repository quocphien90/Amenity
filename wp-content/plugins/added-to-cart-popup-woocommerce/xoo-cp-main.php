<?php
/**
* Plugin Name: WooCommerce added to cart popup (Ajax) 
* Plugin URI: http://xootix.com
* Author: XootiX
* Version: 1.3
* Text Domain: xoo-cp-domain
* Domain Path: /languages
* Author URI: http://xootix.com
* Description: WooCommerce add to cart popup displays popup when item is added to cart without refreshing page.
**/

//Exit if accessed directly
if(!defined('ABSPATH')){
	return; 	
}

$xoo_cp_version = 1.3;

//Load plugin text domain
function xoo_cp_load_txtdomain() {
	$locale = apply_filters( 'plugin_locale', get_locale(), 'xoo-cp' );
	load_textdomain( 'xoo-cp-domain', WP_LANG_DIR . '/xoo-cp-' . $locale . '.mo' );
}
add_action('plugins_loaded','xoo_cp_load_txtdomain');


//Admin Settings
include(plugin_dir_path(__FILE__).'/inc/xoo-cp-admin.php');

//Activation on mobile devices
if(!$xoo_cp_gl_atcem_value){
	if(wp_is_mobile()){
		return;
	}
}

function xoo_cp_enqueue_scripts(){
	global $xoo_cp_version,$xoo_cp_gl_atces_value;
	wp_enqueue_style('xoo-cp-style',plugins_url('/assets/css/xoo-cp-style.css',__FILE__),null,$xoo_cp_version);
	wp_enqueue_style('font-awesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_script('xoo-cp-js',plugins_url('/assets/js/xoo-cp-js.js',__FILE__),array('jquery'),$xoo_cp_version,true);
	wp_dequeue_script('wc-add-to-cart');
	//i8n javascript
	$xoo_cp_js_text = array(
		'added' 	=> __('added successfully.','xoo-cp-domain'),
		'updated'	=> __('updated successfully.','xoo-cp-domain'),
		'removed'	=> __('removed from cart.','xoo-cp-domain'),
		'undo'		=> __('Undo?','xoo-cp-domain')
	);

	wp_localize_script('xoo-cp-js','xoo_cp_localize',array(
		'adminurl'     		=> admin_url().'admin-ajax.php',
		'enshop'			=> $xoo_cp_gl_atces_value,
		'xcp_text'			=> json_encode($xoo_cp_js_text)
		));
}

add_action('wp_enqueue_scripts','xoo_cp_enqueue_scripts',500);


//Get rounded total
function xoo_cp_round($number){
	$thous_sep = get_option( 'woocommerce_price_thousand_sep' );
	$dec_sep   = get_option( 'woocommerce_price_decimal_sep' );
	$decimals  = get_option( 'woocommerce_price_num_decimals' );
	return number_format( $number, $decimals, $dec_sep, $thous_sep );
}


//Get price with currency
function xoo_cp_with_currency($price){
	$price 	  = xoo_cp_round($price);
	$format   = get_option( 'woocommerce_currency_pos' );
	$csymbol  = get_woocommerce_currency_symbol();

	switch ($format) {
		case 'left':
			$currency = $csymbol.$price;
			break;

		case 'left_space':
			$currency = $csymbol.' '.$price;
			break;

		case 'right':
			$currency = $price.$csymbol;
			break;

		case 'right_space':
			$currency = $price.' '.$csymbol;
			break;

		default:
			$currency = $csymbol.$price;
			break;
	}
	return $currency;
}


//Popup HTML
function xoo_cp_popup(){
	global $woocommerce;
	$cart_url 		= $woocommerce->cart->get_cart_url();
	$checkout_url 	= $woocommerce->cart->get_checkout_url();
	?>
	<div class="xoo-cp-opac"></div>
	<div class="xoo-cp-modal">
		<div class="xoo-cp-container">
			<div class="xoo-cp-outer">
			<div class="xoo-cp-cont-opac"></div>
				<div class="xcp-outspin">
						<i class="fa fa-spinner" aria-hidden="true"></i>
				</div>
			</div>
			<i class="fa fa-times xoo-cp-close" aria-hidden="true"></i>

			<div class="xoo-cp-atcn"></div>

			<div class="xoo-cp-content"></div>
				
			<div class="xoo-cp-btns">
				<a class="xoo-cp-btn-vc xcp-btn" href="<?php echo $cart_url; ?>"><?php _e('View Cart','xoo-cp-domain'); ?></a>
				<a class="xoo-cp-btn-ch xcp-btn" href="<?php echo $checkout_url; ?>"><?php _e('Checkout','xoo-cp-domain'); ?></a>
				<a class="xoo-cp-close xcp-btn"><?php _e('Continue Shopping','xoo-cp-domain'); ?></a>
			</div>
		</div>
	</div>
	<?php
}
add_action('wp_head','xoo_cp_popup');

// Ajax Add to cart 
function xoo_cp_cart_ajax(){

	global $woocommerce,$xoo_cp_gl_qtyen_value;
	$product_id 	= intval($_POST['product_id']);
	$variation_id 	= intval($_POST['variation_id']);
	$quantity 		= intval($_POST['quantity']);
	$quantity		= $quantity ? $quantity : 1;

	if($variation_id){
		$attribute_values = wc_get_product_variation_attributes($variation_id);
		$cart_success = $woocommerce->cart->add_to_cart($product_id,$quantity,$variation_id,$attribute_values );
	}

	elseif($variation_id === 0){
		$cart_success = $woocommerce->cart->add_to_cart($product_id,$quantity);
	}

	//Successfully added to cart.
	if($cart_success){
		$cart_item_key  = $cart_success;
		$cart_data		= $woocommerce->cart->get_cart();
		$cart_item_data = $cart_data[$cart_item_key];
		$item_cart_qty	= $cart_item_data['quantity']; 

		if($variation_id){
			$product 	= new WC_product_variation($variation_id);
			$attributes = wc_get_formatted_variation($product);
		}
		else{
			$product 	= new WC_product($product_id);
		}
		
		$product_title 	= $product->get_title();
		$product_price	= $product->get_price();
		$product_image 	= $product->get_image('shop_thumbnail');
		$is_sold_single = $product->is_sold_individually();
		$product_total  =  $product_price*$item_cart_qty;

		$html  = '<table class="xoo-cp-pdetails clearfix" data-cp="'.htmlentities(json_encode(array('key' => $cart_item_key, 'pname' => $product_title))).'">';
		$html .= '<tr>';
		$html .= '<td class="xoo-cp-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></td>';
		$html .= '<td class="xoo-cp-pimg">'.$product_image.'</td>';
		$html .= '<td class="xoo-cp-ptitle"><a href="'.get_permalink($product_id).'">'.$product_title.'</a>';

		if(isset($attributes)){
			$html .= '<div class="xoo-cp-variations">'.$attributes.'</div>';
		}

		$html .= '<td class="xoo-cp-pprice">'.xoo_cp_with_currency($product_price).'</td>';

		if(!$xoo_cp_gl_qtyen_value){
			$disabled = 'disabled';
		}
		else{
			$disabled = '';
		}

		$html .= '<td class="xoo-cp-qtybox">';

		if(!$is_sold_single){
			$html .= '<i class="fa fa-minus-circle xcp-minus xcp-chng" aria-hidden="true"></i>';
			$html .= '<input type="number" class="xoo-cp-qty" value="'.$item_cart_qty.'" '.$disabled.'>';
			$html .= '<i class="fa fa-plus-circle xcp-plus xcp-chng" aria-hidden="true"></i>';
		}
		else{
			$html .= '<span class="xcp-one-qty">'.$item_cart_qty.'</span>';
		}

		$html .= '</td>';

		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<div class="xoo-cp-ptotal"><span class="xcp-totxt">'.__('Total','xoo-cp-domain').': </span><span class="xcp-ptotal">'.xoo_cp_with_currency($product_total).'</span></div>';

		$ajax_fragm = xoo_cp_ajax_fragments();

		wp_send_json(array('pname' => $product_title , 'cp_html' => "$html" , 'ajax_fragm' => $ajax_fragm));
	}
	else{
		if(wc_notice_count('error') > 0){
    		echo wc_print_notices();
		}
  	}
	die();
}



add_action('wp_ajax_xoo_cp_cart_ajax','xoo_cp_cart_ajax');
add_action('wp_ajax_nopriv_xoo_cp_cart_ajax','xoo_cp_cart_ajax');

//Ajax change in cart
function xoo_cp_change_ajax(){
	global $woocommerce;
	$cart_key = sanitize_text_field($_POST['cart_key']);
	$new_qty = (int) $_POST['new_qty'];

	$cart_data		= $woocommerce->cart->get_cart();
	$cart_item_data = $cart_data[$cart_key];
	$item_cart_qty	= $cart_item_data['quantity']; 
	$product_id 	= $cart_item_data['product_id'];
	$variation_id 	= $cart_item_data['variation_id'];
	
	if($variation_id){
		$product = new WC_product_variation($variation_id);
	}
	else{
		$product = new WC_product($product_id);
	}

	if($item_cart_qty > $new_qty){
		$cart_success = $woocommerce->cart->set_quantity($cart_key,$new_qty);
	}
	else{
		$additional_qty = $new_qty - $item_cart_qty;
		if($variation_id){
			$attribute_values	= $cart_item_data['variation'];
			$cart_success 		= $woocommerce->cart->add_to_cart($product_id,$additional_qty,$variation_id,$attribute_values );
		}
		else{
			$cart_success = $woocommerce->cart->add_to_cart($product_id,$additional_qty);
		}
	}

	if($cart_success){
		if($new_qty == 0){
			$ptotal = 0;
		}
		else{
			$product_price 		= $product->get_price();
			$new_cart_data		= $woocommerce->cart->get_cart();
			$new_cart_item_data = $new_cart_data[$cart_key];
			$ptotal 			= xoo_cp_with_currency($new_qty*$product_price);
		}
		$ajax_fragm     = xoo_cp_ajax_fragments();
		$data 			= array('ptotal' => $ptotal ,'ajax_fragm' => $ajax_fragm);
		wp_send_json($data);
	}
	else{
		if(wc_notice_count('error') > 0){
    		echo wc_print_notices();
		}
	}
	die();
}
add_action('wp_ajax_xoo_cp_change_ajax','xoo_cp_change_ajax');
add_action('wp_ajax_nopriv_xoo_cp_change_ajax','xoo_cp_change_ajax');

//Get Ajax refreshed fragments
function xoo_cp_ajax_fragments(){

  	// Get mini cart
    ob_start();

    woocommerce_mini_cart();

    $mini_cart = ob_get_clean();

    // Fragments and mini cart are returned
    $data = array(
        'fragments' => apply_filters( 'woocommerce_add_to_cart_fragments', array(
                'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>'
            )
        ),
        'cart_hash' => apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() )
    );
    return $data;
}

//Options Styles
function xoo_cp_styles(){
	global $xoo_cp_sy_pw_value,$xoo_cp_sy_imgw_value,$xoo_cp_sy_btnbg_value,$xoo_cp_sy_btnc_value,$xoo_cp_sy_btns_value,$xoo_cp_sy_btnbr_value,$xoo_cp_sy_tbc_value,$xoo_cp_sy_tbs_value,$xoo_cp_gl_ibtne_value,$xoo_cp_gl_vcbtne_value,$xoo_cp_gl_chbtne_value,$xoo_cp_gl_qtyen_value;

	$style = '';
	if(!$xoo_cp_gl_ibtne_value || !$xoo_cp_gl_qtyen_value){
		$style .= '.xcp-chng{
			display: none;
		}';
	}

	if(!$xoo_cp_gl_vcbtne_value){
		$style .= 'a.xoo-cp-btn-vc{
			display: none;
		}';
	}

	if(!$xoo_cp_gl_chbtne_value){
		$style .= 'a.xoo-cp-btn-ch{
			display: none;
		}';
	}

	echo "<style>
		.xoo-cp-container{
			max-width: {$xoo_cp_sy_pw_value}px;
		}
		.xcp-btn{
			background-color: {$xoo_cp_sy_btnbg_value};
			color: {$xoo_cp_sy_btnc_value};
			font-size: {$xoo_cp_sy_btns_value}px;
			border-radius: {$xoo_cp_sy_btnbr_value}px;
			border: 1px solid {$xoo_cp_sy_btnbg_value};
		}
		.xcp-btn:hover{
			color: {$xoo_cp_sy_btnc_value};
		}
		td.xoo-cp-pimg{
			width: {$xoo_cp_sy_imgw_value}%;
		}
		table.xoo-cp-pdetails , table.xoo-cp-pdetails tr{
			border: 0!important;
		}
		table.xoo-cp-pdetails td{
			border-style: solid;
			border-width: {$xoo_cp_sy_tbs_value}px;
			border-color: {$xoo_cp_sy_tbc_value};
		}
		{$style}
	</style>";
}	
add_action('wp_head','xoo_cp_styles');
