jQuery(document).ready(function($){

	var xoo_cp_text = JSON.parse(xoo_cp_localize.xcp_text);

	//Add to cart ajax function
	function xoo_cp_atc(atc_btn,variation_id,product_id,quantity){
		$.ajax({
				url: xoo_cp_localize.adminurl,
				type: 'POST',
				data: {action: 'xoo_cp_cart_ajax',
					   variation_id: variation_id,
					   product_id: product_id,
					   quantity: quantity},
			    success: function(response,status,jqXHR){
			    	if(!atc_btn.hasClass('.ajax_add_to_cart')){
			   			atc_btn.find('span.xoo-cp-atc-icon').html('<i class="fa fa-check" aria-hidden="true"></i>');
		   			}
		   			
		   			if(jqXHR.getResponseHeader('content-type').indexOf('text/html') >= 0){
		   				$('.xoo-cp-atcn').html(response);
		   			}

		   			else{
		   				$('.xoo-cp-atcn').html('<div class="xoo-cp-success"><i class="fa fa-check" aria-hidden="true"></i> '+response.pname+' '+xoo_cp_text.added+'</div>');
		   				$('.xoo-cp-content').html(response.cp_html);
		   				xoo_cp_ajax_fragm(response.ajax_fragm);
		   			}

			    	$('.xoo-cp-opac').show();
			    	$('.xoo-cp-modal').addClass('xoo-cp-active');
			    }
			})
	}

	//Refresh ajax fragments
	function xoo_cp_ajax_fragm(ajax_fragm){
		var fragments = ajax_fragm.fragments;
		var cart_hash = ajax_fragm.cart_hash;

		// Block fragments class
		if ( fragments ) {
			$.each( fragments, function( key ) {
				$( key ).addClass( 'updating' );
			});
		}


		// Replace fragments
		if ( fragments ) {
			$.each( fragments, function( key, value ) {
				$( key ).replaceWith( value );
			});
		}

		// Unblock
		$( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();
	}

	//Add to cart on single page
	$(document).on('submit','form.cart',function(e){
		e.preventDefault();
		var atc_btn  = $(this).find('.single_add_to_cart_button');

		if(atc_btn.find('.xoo-cp-atc-icon').length !== 0){
			atc_btn.find('.xoo-cp-atc-icon').html('<i class="fa fa-spinner xoo-cp-spinner" aria-hidden="true"></i><span>');
		}
		else{
			atc_btn.append('<span class="xoo-cp-atc-icon"><i class="fa fa-spinner xoo-cp-spinner" aria-hidden="true"></i><span>');
		}

		if(atc_btn.length === 0){
			var atc_btn = $(this).find('button');
		}
		var variation_id = parseInt($(this).find('[name=variation_id]').val());
		var product_id = parseInt($(this).find('[name=add-to-cart]').val());
		var quantity = parseInt($(this).find('.quantity').find('.qty').val());

		xoo_cp_atc(atc_btn,variation_id,product_id,quantity);//Ajax add to cart
	})

	//Add to cart on shop page
	if(xoo_cp_localize.enshop){
		$('.add_to_cart_button').on('click',function(e){
			var atc_btn = $(this);
			if(atc_btn.hasClass('product_type_variable')){return;}
			e.preventDefault();
			if(atc_btn.find('.xoo-cp-atc-icon').length !== 0){
				atc_btn.find('.xoo-cp-atc-icon').html('<i class="fa fa-spinner xoo-cp-spinner" aria-hidden="true"></i><span>');
			}
			else{
				atc_btn.append('<span class="xoo-cp-atc-icon"><i class="fa fa-spinner xoo-cp-spinner" aria-hidden="true"></i><span>');
			}

			var product_id = atc_btn.data('product_id');
			var variation_id = 0;
			var quantity = 1;
			xoo_cp_atc(atc_btn,variation_id,product_id,quantity);//Ajax add to cart
		})
	}

	//CLose Popup
	function xoo_cp_close_popup(e){
		$.each(e.target.classList,function(key,value){
			if(value == 'xoo-cp-close' || value == 'xoo-cp-modal'){
				$('.xoo-cp-opac').hide();
				$('.xoo-cp-modal').removeClass('xoo-cp-active');
				$('.xoo-cp-atcn , .xoo-cp-content').html('');
			}
		})
	}

	$(document).on('click','.xoo-cp-close',xoo_cp_close_popup);
	$('.xoo-cp-modal').on('click',xoo_cp_close_popup);

	//Ajax function to update cart (In a popup)
	function xoo_cp_update_ajax(cart_key,new_qty,pid){
		return $.ajax({
				url: xoo_cp_localize.adminurl,
				type: 'POST',
				data: {action: 'xoo_cp_change_ajax',
					   cart_key: cart_key, 
					   new_qty: new_qty,
					   pid: pid
					}
			})
	}

	//Update cart (In a popup)
	function xoo_cp_update_cart(_this,new_qty){
		$('.xoo-cp-outer').show();
		var pmain	 = _this.parents('.xoo-cp-pdetails');
		var pdata	 = pmain.data('cp');
		var cart_key = pdata.key;
		var pname 	 = pdata.pname;
		var qty_field= pmain.find('.xoo-cp-qty');
		var old_qty	 = qty_field.attr('xcp-old');  

		xoo_cp_update_ajax(cart_key,new_qty).done(function(response,status,jqXHR){
		 		$('.xoo-cp-outer').hide();
		 		if(jqXHR.getResponseHeader('content-type').indexOf('text/html') >= 0){
		   				$('.xoo-cp-atcn').html(response);
		   				qty_field.val(old_qty);
		   		}
		   		else{
		   			$('.xoo-cp-atcn').html('<div class="xoo-cp-success"><i class="fa fa-check" aria-hidden="true"></i> '+pname+' '+xoo_cp_text.updated+'</div>');
		   			$('.xcp-ptotal').html(response.ptotal);
		   			qty_field.val(new_qty);
		   			xoo_cp_ajax_fragm(response.ajax_fragm);
		   		}
		})
	}

	//Save Value on focus
	$(document).on('focusin','.xoo-cp-qty',function(){
		$(this).attr('xcp-old',$(this).val());
	})
	

	//Qty input on change
	$(document).on('change','.xoo-cp-qty',function(e){
		var _this = $(this);
		var new_qty = parseInt(_this.val());

		if(new_qty <= 0){
			_this.parents('.xoo-cp-pdetails').find('.xoo-cp-remove .fa').trigger('click');
		}
		else{
			xoo_cp_update_cart(_this,new_qty);
		}
	})

	//Plus minus buttons
	$(document).on('click', '.xcp-chng' ,function(){
		var _this = $(this);
		var qty_element = _this.siblings('.xoo-cp-qty');
		qty_element.trigger('focusin');
		var input_qty = parseInt(qty_element.val());

		if(_this.hasClass('xcp-plus')){
			var new_qty	  = input_qty + 1;
		}
		else if(_this.hasClass('xcp-minus')){
			if(input_qty === 1){
				_this.parents('.xoo-cp-pdetails').find('.xoo-cp-remove .fa').trigger('click');
				return;
			}
			else if(input_qty <= 0){
				alert('Invalid');
				return;
			}
			var new_qty = input_qty - 1; 
		}

		xoo_cp_update_cart(_this,new_qty);
	})


	//Remove item from cart
	$(document).on('click','.xoo-cp-remove .fa',function(){
		$('.xoo-cp-outer').show();
		var _this 	 = $(this);
		var pdata	 = _this.parents('.xoo-cp-pdetails').data('cp');
		var cart_key = pdata.key;
		var new_qty	 = 0;
		var pname  	 = pdata.pname; 

		xoo_cp_update_ajax(cart_key,0).done(function(response){
			$('.xoo-cp-content').html('');
			$('.xoo-cp-atcn').html('<div class="xoo-cp-success"><i class="fa fa-check" aria-hidden="true"></i> '+pname+' '+xoo_cp_text.removed+'</div>');
			$('.xoo-cp-outer').hide();
			xoo_cp_ajax_fragm(response.ajax_fragm);
		})
	})

})