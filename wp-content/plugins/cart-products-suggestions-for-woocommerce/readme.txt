=== Products Suggestions for WooCommerce ===
Plugin Name: Products Suggestions for WooCommerce
Contributors: dholovnia, berocket
Donate link: http://berocket.com/product/woocommerce-cart-suggestions
Tags: products suggestion, products promo, woocommerce promotion, woocommerce cart suggestions, cart suggestion, promote my products, suggest products in cart
Requires at least: 4.0
Tested up to: 4.7.4
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Products Suggestions for WooCommerce - promote additional products to your customers. 

== Description ==

Products Suggestions for WooCommerce - promote additional products to your customers. Maybe your customers want some more products from your shop, but don't know about them. You can display products, that will be suggested based on products in the cart. 

* Suggest products by another products in the cart
* Suggest products by categories from products in the cart
* Default suggestions, that uses if another suggestions didn't found
* You can setup suggested products count for cart
* Display suggestions for customers on any pages and any places with widget or shortcode

= Features: =

* Infinite suggestions for products
* Infinite suggestions for categories
* Displays products after the cart table
* Suggestions for all products
* Suggested products for one suggestions selects randomly
* Different priorities for suggestions
* Different types of widgets
* Any count of suggested products


= Additional Features in Paid Plugin: =
* Displays products after the cart totals and before the cart table
* Different types of shortcodes
* Slider type of widget and shortcode


= Paid Plugin Link =
http://berocket.com/product/woocommerce-cart-suggestions

= Demo =
http://woocommerce-grid-list.berocket.com/shop/

= Demo Description =
http://woocommerce-grid-list.berocket.com/cart-suggestions-preview/


= How It Works: =
*check installation*


== Installation ==

In WordPress:

In WordPress:

1. In the admin panel you need to select Plugins-> Add New-> In the upper left corner next to the inscription Add Plugins click Upload Plugin, and then select the downloaded beforehand Fail and click Install Now. / In the admin panel you need to select Plugins-> Add New-> via find the desired plugin and click Install Now.
2. In the Admin panel, select Plugins and click on the plugin called Activate button.


To install manually:

1. It should be a folder with a plug placed in the directory wp-content-> plugins.
2. In the admin panel, select Plugins and click under the plugin name in the Activate button.


== Frequently Asked Questions ==

---

== Screenshots ==
1. General settings

---

== Changelog ==

= 1.0.5 =
* Fix - Categories fixed for WPML
* Fix - Cart add_to_cart issue
* Fix - Better WooCommerce 3 compatibility
* Fix - Premium plugin link on settings page

= 1.0.4 =
* Compatibility with WooCommerce 3.0.0
* Better compatibility with WPML
* Title displayed without products

= 1.0.3 =
* Change styles in admin panel
* Small fixes

= 1.0.2 =
* Better support for PHP5.2

= 1.0.1 =
* First public version
